<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1397174441005" ID="ID_142229176" MODIFIED="1397174911858" TEXT="CT-Login_Sistema001">
<node CREATED="1397174445779" ID="ID_1526886312" MODIFIED="1397174875009" POSITION="right" TEXT="usu&#xe1;rio entra com senha com caracteres especiais">
<icon BUILTIN="smiley-neutral"/>
<node CREATED="1397174487265" ID="ID_1908796666" MODIFIED="1397174783453" TEXT="sistema n&#xe3;o deve fazer login">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1397174615577" ID="ID_162403235" MODIFIED="1397174877121" POSITION="right" TEXT="usu&#xe1;rio entra com espa&#xe7;os em branco">
<icon BUILTIN="smiley-neutral"/>
<node CREATED="1397174487265" ID="ID_602262963" MODIFIED="1397174783453" TEXT="sistema n&#xe3;o deve fazer login">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node CREATED="1397174944296" ID="ID_388957928" MODIFIED="1397174977229" POSITION="right" TEXT="usu&#xe1;rio entra com login valido">
<icon BUILTIN="ksmiletris"/>
<node CREATED="1397174987817" ID="ID_139965089" MODIFIED="1397175003255" TEXT="sistema realiza o login">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</map>
