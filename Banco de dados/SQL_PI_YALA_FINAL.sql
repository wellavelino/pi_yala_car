
-- DELETEM O BANCO DE DADOS EXISTENTE
-- E EXECUTEM TUDO ISSO, RODA LISO...

CREATE database pi_yala_car;
USE pi_yala_car ;

-- -----------------------------------------------------
-- Table `pi_yala_car`.`pessoas`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[pessoas]') AND type in (N'U'))
begin
DROP TABLE pessoas;
end


CREATE TABLE pessoas(
  idPessoa INT  NOT NULL,
  nome VARCHAR(70) NOT NULL,
  cpf VARCHAR(15) NOT NULL,
  telefone VARCHAR(15) NOT NULL,
  sexo CHAR(1) NOT NULL,
  email VARCHAR(55) NOT NULL,
  dataNascimento DATETIME NOT NULL,
  PRIMARY KEY (idPessoa));

  CREATE UNIQUE INDEX unq_cpf ON pessoas (cpf);
  CREATE UNIQUE INDEX unq_email ON pessoas (email);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`clientes`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[clientes]') AND type in (N'U'))
begin
DROP TABLE clientes;
end

CREATE TABLE clientes (
  idCliente INT NOT NULL,
  idPessoa INT NOT NULL,
  endereco VARCHAR(80) NOT NULL,
  cep VARCHAR(9) NOT NULL,
  regiao VARCHAR(25) NOT NULL,
  PRIMARY KEY (idCliente),
  CONSTRAINT fk_clientes_pessoas
    FOREIGN KEY (idPessoa)
    REFERENCES pessoas (idPessoa)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
	
  CREATE INDEX fk_clientes_pessoas ON clientes (idPessoa ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`perfis`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[perfis]') AND type in (N'U'))
begin
DROP TABLE perfis;
end

CREATE TABLE perfis (
  idPerfil INT NOT NULL,
  role VARCHAR(20) NOT NULL,
  descricao VARCHAR(50) NOT NULL,
  dataCriacao TIMESTAMP NOT NULL,
  PRIMARY KEY (idPerfil));

-- -----------------------------------------------------
-- Table `pi_yala_car`.`usuarios`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[usuarios]') AND type in (N'U'))
begin
DROP TABLE usuarios;
end

CREATE TABLE usuarios (
  idUsuario INT NOT NULL,
  idPessoa INT NOT NULL,
  idPerfil INT NOT NULL,
  senha VARCHAR(16) NOT NULL,
  ativo CHAR(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (idUsuario),
  CONSTRAINT fk_usuarios_pessoas
    FOREIGN KEY (idPessoa)
    REFERENCES pessoas (idPessoa)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_usuarios_perfis
    FOREIGN KEY (idPerfil)
    REFERENCES perfis (idPerfil)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

	
 CREATE INDEX fk_usuarios_pessoas ON usuarios (idPessoa ASC);
 CREATE INDEX fk_usuarios_perfis ON usuarios (idPerfil ASC);

-- -----------------------------------------------------
-- Table `pi_yala_car`.`fabricantes`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[fabricantes]') AND type in (N'U'))
begin
DROP TABLE fabricantes;
end

CREATE TABLE fabricantes (
  idFabricante INT  NOT NULL,
  nome VARCHAR(25) NULL,
  PRIMARY KEY (idFabricante));

-- -----------------------------------------------------
-- Table `pi_yala_car`.`modelos`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[modelos]') AND type in (N'U'))
begin
DROP TABLE modelos;
end

CREATE TABLE modelos (
  idModelo INT NOT NULL,
  idFabricante INT NOT NULL,
  nome VARCHAR(20) NOT NULL,
  preco MONEY NOT NULL,
  anoFabricacao VARCHAR(9) NOT NULL,
  PRIMARY KEY (idModelo),
  CONSTRAINT fk_modelos_fabricantes
    FOREIGN KEY (idFabricante)
    REFERENCES fabricantes (idFabricante)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE INDEX fk_modelos_fabricantes ON modelos (idFabricante ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`kits`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[kits]') AND type in (N'U'))
begin
DROP TABLE kits;
end

CREATE TABLE kits (
  idKit INT NOT NULL,
  nome VARCHAR(20) NOT NULL,
  precoTotal MONEY NOT NULL,
  desconto DECIMAL(2,1) NOT NULL,
  basePreco MONEY NOT NULL,
  PRIMARY KEY (idKit));


-- -----------------------------------------------------
-- Table `pi_yala_car`.`veiculos`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[veiculos]') AND type in (N'U'))
begin
DROP TABLE veiculos;
end

CREATE TABLE veiculos (
  idVeiculo INT NOT NULL,
  idModelo INT NOT NULL,
  idKit INT NOT NULL,
  nome VARCHAR(20) NOT NULL,
  categoria VARCHAR(10) NOT NULL,
  quantidadePorta TINYINT NOT NULL DEFAULT 4,
  PRIMARY KEY (idVeiculo),
  CONSTRAINT fk_veiculos_modelos
    FOREIGN KEY (idModelo)
    REFERENCES modelos (idModelo)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_veiculos_kits
    FOREIGN KEY (idKit)
    REFERENCES kits (idKit)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE INDEX fk_veiculos_modelos ON veiculos (idModelo ASC);
  CREATE INDEX fk_veiculos_kits ON veiculos (idKit ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`pedidos`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[pedidos]') AND type in (N'U'))
begin
DROP TABLE pedidos;
end

CREATE TABLE pedidos (
  idPedido INT NOT NULL,
  idCliente INT NOT NULL,
  idVeiculo INT NOT NULL,
  preco MONEY NOT NULL,
  PRIMARY KEY (idPedido),
  CONSTRAINT fk_pedidos_clientes
    FOREIGN KEY (idCliente)
    REFERENCES clientes (idCliente)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_pedidos_veiculos
    FOREIGN KEY (idVeiculo)
    REFERENCES veiculos (idVeiculo)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE INDEX fk_pedidos_clientes ON pedidos (idCliente ASC);
  CREATE INDEX fk_pedidos_veiculos ON pedidos (idVeiculo ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`acessorio`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[acessorios]') AND type in (N'U'))
begin
DROP TABLE acessorios;
end

CREATE TABLE acessorios (
  idAcessorio INT NOT NULL,
  nome VARCHAR(20) NOT NULL,
  categoria VARCHAR(10) NOT NULL,
  precoUnitario MONEY NOT NULL,
  imagem VARCHAR(80) NOT NULL,
  PRIMARY KEY (idAcessorio));


-- -----------------------------------------------------
-- Table `pi_yala_car`.`concessionarias`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[concessionarias]') AND type in (N'U'))
begin
DROP TABLE concessionarias;
end

CREATE TABLE concessionarias (
  idConcessionaria INT NOT NULL,
  idFabricante INT NOT NULL,
  cnpj VARCHAR(18) NOT NULL,
  nome VARCHAR(45) NOT NULL,
  endereco VARCHAR(70) NOT NULL,
  bairro VARCHAR(30) NOT NULL,
  cidade VARCHAR(20) NOT NULL,
  uf CHAR(2) NOT NULL,
  telefoneContato VARCHAR(15) NOT NULL,
  PRIMARY KEY (idConcessionaria),
  CONSTRAINT fk_concessionarias_fabricantes
    FOREIGN KEY (idFabricante)
    REFERENCES fabricantes (idFabricante)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE UNIQUE INDEX unq_cnpj ON concessionarias (cnpj ASC);
  CREATE INDEX fk_concessionarias_fabricantes ON concessionarias (idFabricante ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`acessorioVeiculos`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[acessorioVeiculos]') AND type in (N'U'))
begin
DROP TABLE acessorioVeiculos;
end

CREATE TABLE acessorioVeiculos (
  idAcessorioVeiculo INT NOT NULL,
  idVeiculo INT NOT NULL,
  idAcessorio INT NOT NULL,
  PRIMARY KEY (idAcessorio),
  CONSTRAINT fk_acessorioVeiculos_veiculos
    FOREIGN KEY (idVeiculo)
    REFERENCES veiculos (idVeiculo)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_acessorioVeiculos_acessorio
    FOREIGN KEY (idAcessorio)
    REFERENCES acessorios (idAcessorio)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE INDEX fk_acessorioVeiculos_veiculos ON acessorioVeiculos (idVeiculo ASC);
  CREATE INDEX fk_acessorioVeiculos_acessorio ON acessorioVeiculos (idAcessorio ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`acessorioKits`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[acessorioKits]') AND type in (N'U'))
begin
DROP TABLE acessorioKits;
end

CREATE TABLE acessorioKits (
  idAcessorioKits INT NOT NULL,
  idKit INT NOT NULL,
  idAcessorio INT NOT NULL,
  PRIMARY KEY (idAcessorioKits),
  CONSTRAINT fk_acessorioKits_kits
    FOREIGN KEY (idKit)
    REFERENCES kits (idKit)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_acessorioKits_acessorio
    FOREIGN KEY (idAcessorio)
    REFERENCES acessorios (idAcessorio)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE INDEX fk_acessorioKits_kits ON acessorioKits (idKit ASC);
  CREATE INDEX fk_acessorioKits_acessorio ON acessorioKits (idAcessorio ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`pedidoConcessionarias`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[pedidoConcessionarias]') AND type in (N'U'))
begin
DROP TABLE pedidoConcessionarias;
end

CREATE TABLE pedidoConcessionarias (
  idPedidoConcessionaria INT NOT NULL,
  idConcessionaria INT NOT NULL,
  idPedido INT NOT NULL,
  PRIMARY KEY (idPedidoConcessionaria),
  CONSTRAINT fk_pedidoConcessionarias_concessionarias
    FOREIGN KEY (idConcessionaria)
    REFERENCES concessionarias (idConcessionaria)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT fk_pedidoConcessionarias_pedidos
    FOREIGN KEY (idPedido)
    REFERENCES pedidos (idPedido)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

  CREATE INDEX fk_pedidoConcessionarias_concessionarias ON pedidoConcessionarias (idConcessionaria ASC);
  CREATE INDEX fk_pedidoConcessionarias_pedidos ON pedidoConcessionarias (idPedido ASC);


-- -----------------------------------------------------
-- Table `pi_yala_car`.`modeloCores`
-- -----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[modeloCores]') AND type in (N'U'))
begin
DROP TABLE modeloCores;
end

CREATE TABLE modeloCores (
  idModeloCor INT NOT NULL,
  idModelo INT NOT NULL,
  nome VARCHAR(20) NULL,
  imagem VARCHAR(80) NOT NULL,
  PRIMARY KEY (idModeloCor),
  CONSTRAINT fk_modeloCores_modelos
    FOREIGN KEY (idModelo)
    REFERENCES modelos (idModelo)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

  CREATE INDEX fk_modeloCores_modelos ON modeloCores (idModelo ASC);

