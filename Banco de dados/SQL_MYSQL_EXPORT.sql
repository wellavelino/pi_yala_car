SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `pi_yala_car` ;
CREATE SCHEMA IF NOT EXISTS `pi_yala_car` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `pi_yala_car` ;

-- -----------------------------------------------------
-- Table `pi_yala_car`.`pessoas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`pessoas` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`pessoas` (
  `idPessoa` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(70) NOT NULL,
  `cpf` VARCHAR(15) NOT NULL,
  `telefone` VARCHAR(15) NOT NULL,
  `sexo` CHAR(1) NOT NULL,
  `email` VARCHAR(55) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  PRIMARY KEY (`idPessoa`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`clientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`clientes` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`clientes` (
  `idCliente` INT UNSIGNED NOT NULL,
  `endereco` VARCHAR(80) NOT NULL,
  `cep` VARCHAR(9) NOT NULL,
  `regiao` VARCHAR(25) NOT NULL,
  `idPessoa` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idCliente`),
  INDEX `fk_clientes_pessoas_idx` (`idPessoa` ASC),
  CONSTRAINT `fk_clientes_pessoas`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `pi_yala_car`.`pessoas` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`perfis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`perfis` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`perfis` (
  `idPerfil` INT UNSIGNED NOT NULL,
  `role` VARCHAR(20) NOT NULL,
  `descricao` VARCHAR(50) NOT NULL,
  `dataCriacao` TIMESTAMP NOT NULL,
  PRIMARY KEY (`idPerfil`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`usuarios` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`usuarios` (
  `idUsuario` INT UNSIGNED NOT NULL,
  `senha` VARCHAR(16) NOT NULL,
  `ativo` CHAR(1) NOT NULL DEFAULT '1',
  `idPessoa` INT UNSIGNED NOT NULL,
  `idPerfil` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idUsuario`),
  INDEX `fk_usuarios_pessoas1_idx` (`idPessoa` ASC),
  INDEX `fk_usuarios_perfis1_idx` (`idPerfil` ASC),
  CONSTRAINT `fk_usuarios_pessoas1`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `pi_yala_car`.`pessoas` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_perfis1`
    FOREIGN KEY (`idPerfil`)
    REFERENCES `pi_yala_car`.`perfis` (`idPerfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`fabricantes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`fabricantes` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`fabricantes` (
  `idFabricante` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(25) NULL,
  PRIMARY KEY (`idFabricante`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`modelos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`modelos` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`modelos` (
  `idModelo` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(20) NOT NULL,
  `preco` DOUBLE(10,2) NOT NULL,
  `anoFabricacao` VARCHAR(9) NOT NULL,
  `idFabricante` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idModelo`),
  INDEX `fk_modelos_fabricantes1_idx` (`idFabricante` ASC),
  CONSTRAINT `fk_modelos_fabricantes1`
    FOREIGN KEY (`idFabricante`)
    REFERENCES `pi_yala_car`.`fabricantes` (`idFabricante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`kits`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`kits` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`kits` (
  `idKit` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(20) NOT NULL,
  `precoTotal` DOUBLE(10,2) NOT NULL,
  `desconto` FLOAT(2,1) NOT NULL,
  `basePreco` DOUBLE(10,2) NOT NULL,
  PRIMARY KEY (`idKit`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`veiculos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`veiculos` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`veiculos` (
  `idVeiculo` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(20) NOT NULL,
  `categoria` VARCHAR(10) NOT NULL,
  `quantidadePorta` INT(1) NOT NULL DEFAULT 4,
  `idModelo` INT UNSIGNED NOT NULL,
  `idKit` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idVeiculo`),
  INDEX `fk_veiculos_modelos1_idx` (`idModelo` ASC),
  INDEX `fk_veiculos_kits1_idx` (`idKit` ASC),
  CONSTRAINT `fk_veiculos_modelos1`
    FOREIGN KEY (`idModelo`)
    REFERENCES `pi_yala_car`.`modelos` (`idModelo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_veiculos_kits1`
    FOREIGN KEY (`idKit`)
    REFERENCES `pi_yala_car`.`kits` (`idKit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`pedidos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`pedidos` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`pedidos` (
  `idPedido` INT UNSIGNED NOT NULL,
  `preco` DOUBLE(10,2) NOT NULL,
  `clientes_idCliente` INT UNSIGNED NOT NULL,
  `veiculos_idVeiculo` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idPedido`),
  INDEX `fk_pedidos_clientes1_idx` (`clientes_idCliente` ASC),
  INDEX `fk_pedidos_veiculos1_idx` (`veiculos_idVeiculo` ASC),
  CONSTRAINT `fk_pedidos_clientes1`
    FOREIGN KEY (`clientes_idCliente`)
    REFERENCES `pi_yala_car`.`clientes` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_veiculos1`
    FOREIGN KEY (`veiculos_idVeiculo`)
    REFERENCES `pi_yala_car`.`veiculos` (`idVeiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`acessorio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`acessorio` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`acessorio` (
  `idAcessorio` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(20) NOT NULL,
  `categoria` VARCHAR(10) NOT NULL,
  `precoUnitario` DOUBLE(6,2) NOT NULL,
  `imagem` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`idAcessorio`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`concessionarias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`concessionarias` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`concessionarias` (
  `idConcessionaria` INT UNSIGNED NOT NULL,
  `cnpj` VARCHAR(18) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `endereco` VARCHAR(70) NOT NULL,
  `bairro` VARCHAR(30) NOT NULL,
  `cidade` VARCHAR(20) NOT NULL,
  `uf` CHAR(2) NOT NULL,
  `telefoneContato` VARCHAR(15) NOT NULL,
  `idFabricante` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idConcessionaria`),
  UNIQUE INDEX `cnpj_UNIQUE` (`cnpj` ASC),
  INDEX `fk_concessionarias_fabricantes1_idx` (`idFabricante` ASC),
  CONSTRAINT `fk_concessionarias_fabricantes1`
    FOREIGN KEY (`idFabricante`)
    REFERENCES `pi_yala_car`.`fabricantes` (`idFabricante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`acessorioVeiculos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`acessorioVeiculos` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`acessorioVeiculos` (
  `idAcessorio` INT UNSIGNED NOT NULL,
  `idVeiculo` INT UNSIGNED NOT NULL,
  `idAcessorio` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idAcessorio`),
  INDEX `fk_acessorioVeiculos_veiculos1_idx` (`idVeiculo` ASC),
  INDEX `fk_acessorioVeiculos_acessorio1_idx` (`idAcessorio` ASC),
  CONSTRAINT `fk_acessorioVeiculos_veiculos1`
    FOREIGN KEY (`idVeiculo`)
    REFERENCES `pi_yala_car`.`veiculos` (`idVeiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_acessorioVeiculos_acessorio1`
    FOREIGN KEY (`idAcessorio`)
    REFERENCES `pi_yala_car`.`acessorio` (`idAcessorio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`acessorioKits`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`acessorioKits` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`acessorioKits` (
  `idAcessorioKits` INT UNSIGNED NOT NULL,
  `idKit` INT UNSIGNED NOT NULL,
  `idAcessorio` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idAcessorioKits`),
  INDEX `fk_acessorioKits_kits1_idx` (`idKit` ASC),
  INDEX `fk_acessorioKits_acessorio1_idx` (`idAcessorio` ASC),
  CONSTRAINT `fk_acessorioKits_kits1`
    FOREIGN KEY (`idKit`)
    REFERENCES `pi_yala_car`.`kits` (`idKit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_acessorioKits_acessorio1`
    FOREIGN KEY (`idAcessorio`)
    REFERENCES `pi_yala_car`.`acessorio` (`idAcessorio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`pedidoConcessionarias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`pedidoConcessionarias` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`pedidoConcessionarias` (
  `idPedidoConcessionaria` INT NOT NULL,
  `idConcessionaria` INT UNSIGNED NOT NULL,
  `idPedido` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idPedidoConcessionaria`),
  INDEX `fk_pedidoConcessionarias_concessionarias1_idx` (`idConcessionaria` ASC),
  INDEX `fk_pedidoConcessionarias_pedidos1_idx` (`idPedido` ASC),
  CONSTRAINT `fk_pedidoConcessionarias_concessionarias1`
    FOREIGN KEY (`idConcessionaria`)
    REFERENCES `pi_yala_car`.`concessionarias` (`idConcessionaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidoConcessionarias_pedidos1`
    FOREIGN KEY (`idPedido`)
    REFERENCES `pi_yala_car`.`pedidos` (`idPedido`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pi_yala_car`.`modeloCores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pi_yala_car`.`modeloCores` ;

CREATE TABLE IF NOT EXISTS `pi_yala_car`.`modeloCores` (
  `idModeloCor` INT UNSIGNED NOT NULL,
  `nome` VARCHAR(20) NULL,
  `imagem` VARCHAR(80) NOT NULL,
  `idModelo` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idModeloCor`),
  INDEX `fk_modeloCores_modelos1_idx` (`idModelo` ASC),
  CONSTRAINT `fk_modeloCores_modelos1`
    FOREIGN KEY (`idModelo`)
    REFERENCES `pi_yala_car`.`modelos` (`idModelo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
