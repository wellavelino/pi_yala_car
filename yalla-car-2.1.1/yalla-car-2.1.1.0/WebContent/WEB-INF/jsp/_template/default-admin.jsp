<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="d" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><d:title default="Projeto"/></title>
        
        <link rel="stylesheet"  type="text/css" href="<c:url value="/assets/css/style.default.css"/>"/>  
        
        <script type="text/javascript" src="<c:url value="/assets/js/plugins/jquery.min.1.11.0.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/plugins/jquery-ui-1.8.16.custom.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/plugins/jquery.cookie.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/plugins/jquery.validate.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/plugins/jquery.tagsinput.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/plugins/charCount.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/general.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/forms.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/functions.js"/>"></script>
        
        <script type="text/javascript" src="<c:url value="/assets/js/custom/bootstrap.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/jquery.ui.widget.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/load-image.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/jquery.iframe-transport.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/jquery.fileupload.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/jquery.fileupload-process.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/jquery.fileupload-image.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/assets/js/custom/upload/jquery.fileupload-validate.js"/>"></script>
    </head>
        
    <body class="withvernav">

           <div class="bodywrapper">
               <div class="topheader">
                   <div class="left">
                       <h1 class="logo">yala<span>car</span></h1>

                       <br clear="all" />

                   </div><!--left-->

                   <div class="right">
                       <div class="notification">

                       </div>
                       <div class="userinfo">
                            <c:if test="${validaLogin.logado}">
                                <span>${validaLogin.nome}</span>
                            </c:if>
                       </div><!--userinfo-->

                       <div class="userinfodrop">
                           <c:if test="${validaLogin.logado}">
                               <div class="userdata">
                                   <h4>${validaLogin.nome}</h4>
                                   <span class="email">${validaLogin.email}</span>
                                   <ul>
                                        <c:if test="${validaLogin.perfil eq 'ADMINISTRADOR'}">
                                            <li><a href="<c:url value="/admin/usuario/edit/${validaLogin.userID}"/>">Editar Perfil</a></li>
                                        </c:if>
                                       <li><a href="<c:url value="/admin/login/logout"/>">Logout</a></li>
                                   </ul>
                               </div><!--userdata-->
                           </c:if>
                       </div><!--userinfodrop-->
                   </div><!--right-->
               </div><!--topheader-->


               <div class="header">
                   <ul class="headermenu">
                       <li id="veiculos" ><a   class="menu2" href=""><span class="icon icon-pencil"></span>MENU</a></li>
                      <!-- <li id="pecas" ><a class="menu2" href=""><span class="icon icon-pencil"></span>Pe�as</a></li>
                       <li id="usuario"  ><a  class="menu2"href="#"><span class="icon icon-pencil"></span>Usu�rios</a></li> -->
                   </ul>    
               </div><!--header-->

               <div class="vernav2">
                    <%@ include file="/WEB-INF/jsp/_template/_include/menu.jsp"%>
               </div><!--leftmenu-->

               <div class="centercontent">
                   
                   <c:forEach items="${errors}" var="error">
                    <div class="notibar msgerror">
                        <a class="close"></a>
                        <p>${error.message}</p>
                    </div>
                   </c:forEach>
                   
                   <d:body />
               </div><!-- centercontent -->
           </div><!--bodywrapper-->
       </body>
</html>
