<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<ul>
    <li><a href="#formsub" class="editor">Concessionaria</a>
        <span class="arrow"></span>
        <ul id="formsub">
            <li><a href="<c:url value="/admin/concessionaria/list"/>">Listar Concessionarias</a></li>
            <li><a href="<c:url value="/admin/concessionaria/new"/>">Cadastrar Concessionaria</a></li>
        </ul>
    </li>
</ul>
<ul>
    <li><a href="#error" class="error">Fabricantes</a>
        <span class="arrow"></span>
        <ul id="error">
            <li><a href="<c:url value="/admin/fabricante/list"/>">Listar Fabricantes</a></li>
            <li><a href="<c:url value="/admin/fabricante/new"/>">Cadastrar Fabricante</a></li>
        </ul>
    </li>
</ul>
<ul>
    <li><a href="#addons" class="addons">Modelos</a>
        <span class="arrow"></span>
        <ul id="addons">
            <li><a href="<c:url value="/admin/modelo/list"/>">Listar Modelos</a></li>
            <li><a href="<c:url value="/admin/modelo/new"/>">Cadastrar Modelo</a></li>
        </ul>
    </li>
</ul>
     <ul>
        <li><a href="#addons" class="addons">Modelos</a>
            <span class="arrow"></span>
            <ul id="addons">
                <li><a href="<c:url value="/admin/modelo/list"/>">Listar Modelos</a></li>
                <li><a href="<c:url value="/admin/modelo/new"/>">Cadastrar Modelo</a></li>
            </ul>
        </li>
    </ul>
    <ul>
        <li><a href="#addooooons" class="addons">Veiculos</a>
            <span class="arrow"></span>
            <ul id="addooooons">
                <li><a href="<c:url value="/admin/veiculo/list"/>">Listar Veiculos</a></li>
                <li><a href="<c:url value="/admin/veiculo/new"/>">Cadastar Veiculo</a></li>
            </ul>
        </li>
    </ul>
    <ul>
        <li><a href="#addooons" class="addons">Acessórios</a>
            <span class="arrow"></span>
            <ul id="addooons">
                <li><a href="<c:url value="/admin/acessorio/list"/>">Listar Acessórios</a></li>
                <li><a href="<c:url value="/admin/acessorio/new"/>">Cadastrar Acessório</a></li>
            </ul>
        </li>
    </ul>
    <ul>
        <li><a href="#addoooons" class="addons">Usuario</a>
            <span class="arrow"></span>
            <ul id="addoooons">
                <li><a href="<c:url value="/admin/usuario/list"/>">Listar Usuario</a></li>
                <li><a href="<c:url value="/admin/usuario/new"/>">Cadastar Usuario</a></li>
            </ul>
        </li>
    </ul>
    <ul>

        <li><a href="<c:url value="/admin/welcome"/>">HOME</a></li>

    </ul>   


    <br /><br />