<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="d" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><d:title default="Projeto"/></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<c:url value="/assets/client/css/libs/normalize.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/assets/client/css/libs/main.css"/>">
        <link rel="stylesheet" href="<c:url value="/assets/client/css/libs/slider.css"/>">

        <script>window.jQuery || document.write('<script src="<c:url value="/assets/js/plugins/jquery.min.1.11.0.js"/>"><\/script>')</script>
    </head>
    <body>
        <main role="main">
            <div class="container">
                <header class="container-header">
                    <h1 class="logo-yala"><a href="<c:url value="/"/>" class="ir">Yala Car</a></h1>
                </header>
                <d:body />
            </div>
        </main>
        <script src="<c:url value="/assets/client/js/libs/default.js"/>"></script>
    </body>
</html>