<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div class="slider">
    <ul class="content-veiculos">
        <c:forEach items="${listVeiculos}" var="v">
            <c:forEach items="${v.modelosCollection}" var="m">
                <li>
                    <a href="<c:url value="/montVeiculo/${m.idModelo}" />">
                        <img src="<c:url value="/uploads/${m.thumb}"/>">
                        <p class="content-info">
                            ${v.nome} - ${m.nome}
                        </p>
                    </a>
                </li>
            </c:forEach>
        </c:forEach>
    </ul>
</div>
<script src="<c:url value="/assets/client/js/libs/slider.js"/>"></script>