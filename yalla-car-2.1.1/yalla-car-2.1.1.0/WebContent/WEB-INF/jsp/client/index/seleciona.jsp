<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<link rel="stylesheet" href="<c:url value="/assets/client/css/selecionaVeiculo/selecionaVeiculo.css"/>">
<section class="container-selecionaVeiculo">
        <div class="container-carousel-marcas">
            <div class="content-marcas">
                <ul>
                    <c:forEach items="${fabricanteList}" var="f">
                        <li>
                            <a href="<c:url value="/veiculoConf/${f.idFabricante}"/>">
                                <img src="<c:url value="/uploads/${f.imagem}"/>" alt="${f.nome}">
                            </a>
                        </li>
                    </c:forEach>
                </ul>                
            </div>
        </div>    
</section>
<script src="<c:url value="/assets/client/js/selecionar_veiculo/main.js"/>"></script>