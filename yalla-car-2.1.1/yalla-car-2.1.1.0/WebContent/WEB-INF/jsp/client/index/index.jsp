<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<section class="container-selecionaVeiculo">
    <link rel="stylesheet" href="<c:url value="/assets/client/css/home/home.css"/>">
    <div class="item-info-monte-car">
        <p class="text-monte-car">
            Monte<br /> 
            seu carro
        </p>
        <div class="seta"></div>
        <a href="<c:url value="/seleciona" />" class="btn btn-home">Clique aqui!</a>
    </div>
    <div class="logo-footer"><div>
</section>