<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<link rel="stylesheet" href="<c:url value="/assets/client/css/configuraVeiculo/configuraVeiculo.css"/>">
<section class="container-configura-veiculo">
    <form>
        <div class="slider-veiculo">
            <ul class="content-veiculos">
                <c:forEach items="${modelos}" var="mc">
                    <li>
                        <p>
                            <img src="<c:url value="/uploads/${mc.imagem}"/>" alt="${mc.nome}">
                            <span class="id hide">${mc.idModeloCor}</span>
                        </p>
                        
                        <p class="value-car valueVeiculo">
                            R$ ${mc.idModelo.preco}
                        </p>
                        <p class="name-veiculo">
                            <span class="quadrado"></span>
                            <span class="name">${mc.idModelo.veiculo.nome} <br /> ${mc.idModelo.nome}</span> 
                            <span class="quadrado"></span>
                        </p>
                        <input class="valueVeiculo" type="hidden" name="" value="${mc.idModelo.preco}" />
                    </li>
                </c:forEach>
            </ul>
        </div>
        <div class="container-kits">
            <div class="slider-kits">
                <ul class="content-veiculos">
                    <c:forEach items="${kits}" var="ks" >
                        <li class="kits">
                            <h2>${ks.nome}</h2>
                            <div class="content-info-kits">
                                <span class="id hide">0</span>
                                <ul>
                                    <c:forEach items="${ks.acessorioKitsCollection}" var="ac" >
                                        <li>${ac.acessorio.nome}</li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <input  name="Kit.idKit" type="hidden" value="${ks.idKit}" />                            
                            <input name="Kit.idKit" class="value-add" type="hidden" value="${ks.precoTotal}" />
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="content-info-kits acessorios hide">
                <ul>
                    <c:forEach items="${acessorios}" var="a">
                        <li>
                            <input type="checkbox" name="acessorios.idAcessorio" value="${a.idAcessorio}">  
                            ${a.nome}
                            <input type="hidden" class="value-add" value="${a.precoUnitario}"/>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="content-btns">
                <a class="btn btn-acessorio btn-button" href="#">Acessorios Extras</a>
                <button class="btn btn-finalizar">Finalizar</button>
            </div>
        </div>
        <div class="containner-finalizar hide">
            <ul>
                <li>
                    <p>
                        N�o gostou
                        do kit? =(
                    </p>
                    <p>
                        <a class="btn btn-button">Retornar</a>
                    </p>
                </li>
                <li>
                    <p>
                        Gostou
                        do Kit =D
                    </p>
                    <p>
                        <button class="btn btn-finalizar">Finalizar</button>
                    </p>
                </li>
            </ul>
        </div>
        <div class="divisao"></div>
        <div class="container-description">
            <div class="content-info">
                <h4 class="title-description">Ficha t�cnica</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu ipsum tortor. Duis dui tellus, tincidunt ac vehicula ut, tincidunt ut turpis. Morbi mattis aliquam quam vitae facilisis. Sed tempor metus ac risus aliquam condimentum. Aenean eu sem nisi, eu dictum libero. 
                </p>
            </div>
        </div>
    </form>
</section>
<footer>
    <div class="text-center">
        <a class="voltar" href="#">Voltar</a>
    </div>
</footer>
<script src="<c:url value="/assets/client/js/libs/slider.js"/>"></script>
<script src="<c:url value="/assets/client/js/montar_veiculo/montar_veiculo.js"/>"></script>