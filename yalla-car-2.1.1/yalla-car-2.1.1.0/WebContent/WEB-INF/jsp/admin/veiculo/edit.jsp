<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div class="pageheader notab">
    <h1 class="pagetitle">Editar Ve�culo</h1>
    <span class="pagedesc">formul�rio dispon�vel para edi��o de ve�culos</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="basicform" class="subcontent">

        <form class="stdform stdform2" method="POST" action="<c:url value="/admin/veiculo/update/${veiculo.idVeiculo}"/>">            

            <p>
                <label>Nome</label>
                <span class="field"><input type="text" name="veiculo.nome" class="longinput" value="${veiculo.nome}"/></span>
            </p>

            <p>
                <label>Categoria</label>
                <span class="field"><input type="text" name="veiculo.categoria" class="longinput" value="${veiculo.categoria}"/></span>
            </p>
            <p>
                <label>Quantidade de portas</label>
                <span class="field"><input type="text" name="veiculo.quantidadePorta" class="smallinput" value="${veiculo.quantidadePorta}"/></span>
            </p>  
            <p>
                <label>Fabricante</label>
                <span class="field">
                    <select name="veiculo.fabricante.idFabricante">  
                        <c:forEach items="${listaFabricantes}" var="f">  
                            <option <c:if test="${veiculo.fabricante.idFabricante == f.idFabricante}"> selected="selected"</c:if>  
                                value="<c:out value="${f.idFabricante}" />">${f.nome}</option> 
                        </c:forEach>  
                    </select>
                </span>
            </p>
             
            <input type="hidden" name="_method" value="PUT"/>
            
            <p class="stdformbutton">
                <button type="submit" class="submit radius2">Atualizar</button>
                <input type="reset" class="reset radius2" value="Resetar Formul�rio"/>
            </p>

        </form>

        <br/>

    </div>
</div><!--contentwrapper-->