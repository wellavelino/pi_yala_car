<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div class="pageheader notab">
    <h1 class="pagetitle">Cadastro de Ve�culo</h1>
    <span class="pagedesc">formul�rio dispon�vel para cadastro de novos ve�culos</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="basicform" class="subcontent">

        <form class="stdform stdform2" method="post" action="<c:url value="/admin/veiculo/add"/>">

            <p>
                <label>Nome</label>
                <span class="field"><input type="text" name="veiculo.nome" class="longinput" value="${veiculo.nome}"/></span>
            </p>

            <p>
                <label>Categoria</label>
                <span class="field"><input type="text" name="veiculo.categoria" class="longinput" value="${veiculo.categoria}"/></span>
            </p>
            <p>
                <label>Quant. Portas</label>
                <span class="field"><input type="text" name="veiculo.quantidadePorta" class="smallinput" value="${veiculo.quantidadePorta}"/></span>
            </p>  
            <p>
                <label>Fabricantes</label>
                <span class="field">
                    <select name="veiculo.fabricante.idFabricante">  
                        <c:forEach items="${listaFabricantes}" var="f">  
                            <option <c:if test="${veiculo.fabricante.idFabricante == f.idFabricante}"> selected="selected"</c:if>  
                                value="<c:out value="${f.idFabricante}" />">${f.nome}</option> 
                        </c:forEach>  
                    </select>
                </span>
            </p>
            <p class="stdformbutton">
                <button class="submit radius2">Cadastrar</button>
                <input type="reset" class="reset radius2" value="Resetar Formul�rio"/>
            </p>

        </form>

        <br/>

    </div>
</div><!--contentwrapper-->