<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
              
    <div class="pageheader notab">
        <h1 class="pagetitle">Listagem de Ve�culos</h1>
        <span class="pagedesc">busca de ve�culos cadastrados</span>
    </div>

    <div id="contentwrapper" class="contentwrapper">

        <div class="contenttitle2">
            <h3>Ve�culos</h3>
        </div><!--contenttitle-->

        <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            <colgroup>
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
            </colgroup>
            <thead>
                <tr>
                    <th class="head1">NOME</th>
                    <th class="head0">CATEGORIA</th>
                    <th class="head0">QUANTIDADE DE PORTAS</th>
                    <th class="head0">A��ES</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${veiculoList}" var="c">
                  <tr>
                    <td>${c.nome}</td>
                    <td>${c.categoria}</td>
                    <td>${c.quantidadePorta}</td>
                    <td class="center"><a href="<c:url value="/admin/veiculo/edit/${c.idVeiculo}"/>">Editar</a> | <a href="<c:url value="/admin/veiculo/remove/${c.idVeiculo}"/>">Remover</a></td>
                  </tr>          
                </c:forEach>
            </tbody>
        </table>
    </div>
