<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
              
<div class="pageheader notab">
    <h1 class="pagetitle">Listagem de Fabricantes</h1>
    <span class="pagedesc">busca de fabricantes cadastrados</span>
</div>

<div id="contentwrapper" class="contentwrapper">

    <div class="contenttitle2">
        <h3>Fabricantes</h3>
    </div><!--contenttitle-->

    <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
        <colgroup>
            <col class="con0" />
            <col class="con1" />
            <col class="con0" />
            <col class="con1" />
            <col class="con0" />
        </colgroup>
        <thead>
            <tr>
                <th class="head1">NOME</th>
                <th class="head0">A��ES</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${fabricanteList}" var="c">
              <tr>
                <td>${c.nome}</td>
                <td class="center"><a href="<c:url value="/admin/fabricante/edit/${c.idFabricante}"/>">Editar</a> | <a href="<c:url value="/admin/fabricante/remove/${c.idFabricante}"/>">Remover</a></td>
              </tr>          
            </c:forEach>
        </tbody>
    </table>
</div>
