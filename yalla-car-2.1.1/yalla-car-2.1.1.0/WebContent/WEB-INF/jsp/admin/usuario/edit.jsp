<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 

<div class="pageheader notab">
    <h1 class="pagetitle">Cadastrar Usuario</h1>
    <span class="pagedesc">formulário disponível para cadastro de novos usuarios</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="basicform" class="subcontent">

        <form id="formUsuario" class="stdform stdform2" method="post" action="<c:url value="/admin/usuario/update/${usuario.idUsuario}"/>">

            <p>
                <label>Nome</label>
                <span class="field"><input type="text" name="pessoa.nome" class="longinput" value="${usuario.pessoa.nome}"/></span>
            </p>

            <p>
                <label>CPF</label>
                <span class="field"><input type="text" name="pessoa.cpf" class="smallinput" value="${usuario.pessoa.cpf}"/></span>
            </p>

            <p>
                <label>Telefone</label>
                <span class="field"><input type="text" name="pessoa.telefone" class="smallinput" value="${usuario.pessoa.telefone}"/></span>
            </p>

            <p>
                <label>Sexo</label>
                <span class="field">
                    <select name="pessoa.sexo">
                        <c:forEach items="${comboSexos}" var="ce">  
                            <option <c:if test="${fn:contains(usuario.pessoa.sexo, ce[1])}"> selected="selected"</c:if>  
                                value="<c:out value="${ce[1]}" />">${ce[0]}</option>  
                        </c:forEach> 
                    </select>
                </span>
            </p> 

            <p>
                <label>Email</label>
                <span class="field"><input type="text" name="pessoa.email" class="smallinput" value="${usuario.pessoa.email}"/></span>
            </p> 

            <p>
                <label>Data Nascimento</label>
                <span class="field">                       
                    <input id="calendar" type="text" name="pessoa.dataNascimento"
                           value="<fmt:formatDate value="${usuario.pessoa.dataNascimento}" type="date" pattern="dd/MM/yyyy"/>"
                           class="width100 hasDatepicker"/> 
                </span>
            </p>

            <p>
                <label>Login</label>
                <span class="field"><input type="text" name="usuario.login" class="smallinput" value="${usuario.login}"/></span>
            </p> 

            <p>
                <label>Senha</label>
                <span class="field"><input id="pass" type="password" name="usuario.senha" class="smallinput" value="${usuario.senha}" /></span>
            </p>

            <p>
                <label>Conf. Senha</label>
                <span class="field"><input id="passconf" type="password" type="password" equalTo="#senha" class="smallinput" value="${usuario.senha}"/></span>
            </p>
            
            <p>
                <label>Perfil</label>
                <span class="field"> 
                    <select name="tperfil">
                        <c:forEach items="${comboPerfis}" var="ce">  
                            <option  <c:if test="${fn:contains(usuario.perfil, ce[0])}"> selected="selected"</c:if> 
                                value="<c:out value="${ce[1]}" />">${ce[0]}</option>  
                        </c:forEach>  
                    </select>
                </span>
            </p>   

            <p>
                <label>Status</label>
                <span class="formwrapper">
                    <br/>
                    <span>
                        <input type="checkbox" name="usuario.ativo"
                                <c:if test="${usuario.ativo == true}"> checked </c:if>/>
                    </span> Ativo ?
                    <br/>
                    <br/>
                </span>
            </p>
            
            <input type="hidden" name="_method" value="PUT"/>
            <input type="hidden" name="pessoa.idPessoa" value="${usuario.pessoa.idPessoa}"/>

            <p class="stdformbutton">
                <button class="submit radius2">Atualizar</button>
            </p>

        </form>

        <br/>

    </div>
</div><!--contentwrapper-->