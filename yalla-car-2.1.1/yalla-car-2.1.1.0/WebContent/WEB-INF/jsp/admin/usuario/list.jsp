<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

    <div class="pageheader notab">
        <h1 class="pagetitle">Listagem de Usu�rios</h1>
        <span class="pagedesc">busca de usu�rios cadastrados</span>
    </div>

    <div id="contentwrapper" class="contentwrapper">

        <div class="contenttitle2">
            <h3>Usu�rios</h3>
        </div><!--contenttitle-->

        <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
            </colgroup>
            <thead>
                <tr>
                    <th class="head1">LOGIN</th>
                    <th class="head1">NOME</th>
                    <th class="head1">EMAIL</th>
                    <th class="head0">A��ES</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${usuarioList}" var="c">
                  <tr>
                    <td>${c.login}</td>
                    <td>${c.pessoa.nome}</td>
                    <td>${c.pessoa.email}</td>
                    <td class="center"><a href="<c:url value="/admin/usuario/edit/${c.idUsuario}"/>">Editar</a> | <a href="<c:url value="/admin/usuario/remove/${c.idUsuario}/${c.pessoa.idPessoa}"/>">Remover</a></td>
                  </tr>          
                </c:forEach>
            </tbody>
        </table>
    </div>
