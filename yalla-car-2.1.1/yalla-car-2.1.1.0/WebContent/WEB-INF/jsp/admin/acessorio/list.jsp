<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<div class="pageheader notab">
    <h1 class="pagetitle">Listagem de Acess�rio</h1>
    <span class="pagedesc">formul�rio dispon�vel para listagem de acess�rios</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div class="contenttitle2">
        <h3>Acessorios</h3>
    </div><!--contenttitle-->



    <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
        <colgroup>
            <col class="con0" />
            <col class="con1" />
            <col class="con0" />
            <col class="con1" />
            <col class="con0" />
        </colgroup>
        <thead>
            <tr>
                <th class="head0">NOME</th>
                <th class="head1">CATEGORIA</th>
                <th class="head0">PRECO UNITARIO</th>
                <th class="head0">A��ES</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${acessorioList}" var="a">
                <tr>
                    <td>${a.nome}</td>
                    <td>${a.categoria}</td>
                    <td>${a.precoUnitario}</td>
                    <td class="center"><a href="<c:url value="/admin/acessorio/edit/${a.idAcessorio}"/>">Editar</a> | <a href="<c:url value="/admin/acessorio/remove/${a.idAcessorio}"/>">Remover</a></td>
                </tr>          
            </c:forEach>
        </tbody>
    </table>
</div>
