<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div class="pageheader notab">
    <h1 class="pagetitle">Editar Acess�rio</h1>
    <span class="pagedesc">formul�rio dispon�vel para edi��o de acess�rios</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="basicform" class="subcontent">

        <form class="stdform stdform2" method="post" 
              action="<c:url value="/admin/acessorio/update/${acessorio.idAcessorio}"/>">

            <p>
                <label>Nome</label>
                <span class="field"><input type="text" name="acessorio.nome" 
                                           class="longinput" value="${acessorio.nome}"/></span>
            </p>

            <p>
                <label>Categoria</label>
                <span class="field"><input type="text" name="acessorio.categoria" 
                                           class="longinput" value="${acessorio.categoria}"/></span>
            </p>

            <p>
                <label>Pre�o Unit.</label>
                <span class="field"><input type="text" name="acessorio.precoUnitario" 
                                           class="smallinput" value="${acessorio.precoUnitario}"/></span>
            </p>     
            
            <p>
                <label>Imagem</label>
                <span class="field">                                     
                    <input id="fileupload" type="file" name="imagem"/>
                </span>
            </p>  
            
            <input id="ImageName" type="hidden" name="oldImagem" value="${acessorio.imagem}"/>     
            <input id="ImageNameNew" type="hidden" name="acessorio.imagem" value="${acessorio.imagem}" />   
             
            <input type="hidden" name="_method" value="PUT"/>

            <p class="stdformbutton">
                <button class="submit radius2">Atualizar</button>
                <input type="reset" class="reset radius2" value="Resetar Formul�rio"/>
            </p>

        </form>

        <br/>              
                                
        <!-- The global progress bar -->
        <div class="progress" style="display:none"> 
            <div id="progress" class="bar">     
                <div class="value bluebar" style="width: 0%;"></div>
            </div>
        </div> 
  
        <!-- The container for the uploaded files -->
        <div id="files" class="files" style="overflow: auto;">
            <div class="gallerywrapper" style="margin-top:20px;padding:0;">
                <ul class="imagelist">  
                </ul>                    
            </div>
        </div>      
        
        <div class="contenttitle2">
            <h3>Imagem Enviada</h3>
            <small>ao enviar uma nova imagem, a atual ser� substitu�da.</small>
        </div>

        <div class="contenttitle2 overwrite_div_for_image">                        
             <ul class="listfile">
                <li>
                    <a class="image" href="<c:url value="/admin/acessorio/${acessorio.idAcessorio}/imagem"/>">
                        <span class="img">
                            <img src="<c:url value="/admin/acessorio/${acessorio.idAcessorio}/imagem"/>" alt=""/>
                        </span>
                    </a>
                </li>
             </ul>
        </div>    

        <br/>

    </div>
</div><!--contentwrapper-->

<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
jQuery(function () {       
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '<c:url value="/admin/image/upload"/>';     
            
    jQuery('#fileupload').fileupload({     
        
        url: url,
        dataType: 'json', 
        autoUpload: true, 
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,  
        maxFileSize: 5000000, // 5 MB
        previewMaxWidth: 200,
        previewMaxHeight: 200, 
        previewCrop: true  
        
    }).on('fileuploadadd', function (e, data) { 
        
        data.context = jQuery('<li/>').appendTo('#files ul.imagelist'); 
        jQuery.each(data.files, function (index, file) { 
            var node = jQuery('<span/>')
                    .append(jQuery('<a class="name ajax cboxElement"/>').text(file.name))
                    .append(jQuery('<a class="delete"/>'));
            node.appendTo(data.context); 
        });
        
    }).on('fileuploadprocessalways', function (e, data) {
        
        var index = data.index, 
            file = data.files[index],
            node = jQuery(data.context.children()[index]);
            
        if (file.error) {
            alert(file.error); 
            node.parent().remove();
            return;
        }           
            
        if (file.preview) {  
            node
                .prepend('<br/><br/>')  
                .prepend(file.preview);     
        }
        
    }).on('fileuploadprogressall', function (e, data) {
        
        var progress = parseInt(data.loaded / data.total * 100, 10);
        jQuery('.progress').show();
        jQuery('#progress.bar').find('.bluebar').css('width', progress + '%');
        
    }).on('fileuploaddone', function (e, data) {
        
        jQuery('.progress').hide();
        jQuery('#progress.bar').find('.bluebar').css('width','0%');
        console.log(data); 
        console.log(data.result); 
        console.log(data.result.files); 
        jQuery.each(data.result, function (index, file) {
            if (file.url) {
                var link = jQuery('<a>') 
                    .attr('target', '_blank') 
                    .prop('href', file.deleteUrl); 
                jQuery(data.context.children()[index])                 
                    .find('.delete')
                    .wrap(link);            
                jQuery('#ImageNameNew').val(file.name);
            } else if (file.error) { 
                alert(file.error);
                return;
            }
        });
        
    }).on('fileuploadfail', function (e, data) {   
        
        jQuery.each(data.files, function (index, file) { 
            alert(error);
        });
        
    }).prop('disabled', !jQuery.support.fileInput) 
        .parent().addClass(jQuery.support.fileInput ? undefined : 'disabled');
});
</script>
