<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
              
    <div class="pageheader notab">
        <h1 class="pagetitle">Listagem de Kits</h1>
        <span class="pagedesc">busca de kits cadastrados</span>
    </div>

    <div id="contentwrapper" class="contentwrapper">

        <div class="contenttitle2">
            <h3>Kits</h3>
        </div><!--contenttitle-->

        <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
            </colgroup>
            <thead>
                <tr>
                    <th class="head0">NOME</th>
                    <th class="head1">PRE�O TOTAL</th>
                    <th class="head0">DESCONTO</th>
                    <th class="head0">BASE PRE�O</th>
                    <th class="head0">A��ES</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${kitList}" var="c">
                  <tr>
                    <td>${c.nome}</td>
                    <td>${c.precoTotal}</td>
                    <td>${c.desconto}</td>
                    <td>${c.basePreco}</td>
                    <td class="center"><a href="<c:url value="/admin/kit/edit/${c.idKit}"/>">Editar</a> | <a href="<c:url value="/admin/kit/remove/${c.idKit}"/>">Remover</a></td>
                  </tr>          
                </c:forEach>
            </tbody>
        </table>
    </div>
