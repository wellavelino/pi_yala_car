<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="pageheader notab">
    <h1 class="pagetitle">Cadastro de Kit</h1>
    <span class="pagedesc">formul�rio dispon�vel para cadastro de novos kits</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="basicform" class="subcontent">

        <form class="stdform stdform2" method="POST" action="<c:url value="/admin/kit/add"/>">

            <p>
                <label>Nome</label>
                <span class="field"><input type="text" name="kit.nome" class="longinput" value="${kit.nome}"/></span>
            </p>

            <p>
                <label>Pre�o Total</label>
                <span class="field"><input type="text" name="kit.precoTotal" class="smallinput" value="${kit.precoTotal}"/></span>
            </p>

            <p>
                <label>Desconto</label>
                <span class="field"><input type="text" name="kit.desconto" class="smallinput" value="${kit.desconto}"/></span>
            </p>

            <p>
                <label>Pre�o Base</label>
                <span class="field"><input type="text" name="kit.basePreco" class="smallinput" value="${kit.basePreco}"/></span>
            </p>

            <p>
                <label>Acess�rios</label>
                <span class="field">
                    <select name="acessorio[].idAcessorio" multiple="multiple" size="5"> 
                        <c:forEach items="${listaAcessorios}" var="f"> 
                            <option value="<c:out value="${f.idAcessorio}" />">${f.nome}</option>
                        </c:forEach>  
                    </select>
                </span> 
            </p>

            <p class="stdformbutton">
                <button class="submit radius2">Cadastrar</button>
                <input type="reset" class="reset radius2" value="Resetar Formul�rio"/>
            </p>

        </form>

        <br/>

    </div>
</div><!--contentwrapper-->
