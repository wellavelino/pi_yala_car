<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
              
    <div class="pageheader notab">
        <h1 class="pagetitle">Listagem de Modelos</h1>
        <span class="pagedesc">busca de modelos cadastrados</span>
    </div>

    <div id="contentwrapper" class="contentwrapper">

        <div class="contenttitle2">
            <h3>Ve�culos</h3>
        </div><!--contenttitle-->

        <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            <colgroup>
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
            </colgroup>
            <thead>
                <tr>
                    <th class="head1">NOME</th>
                    <th class="head0">PRE�O</th>
                    <th class="head0">ANO DE FABRICA��O</th>
                    <th class="head0">A��ES</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${modeloList}" var="c">
                  <tr>
                    <td>${c.nome}</td>
                    <td>${c.preco}</td>
                    <td>${c.anoFabricacao}</td>
                    <td class="center"><a href="<c:url value="/admin/modelo/edit/${c.idModelo}"/>">Editar</a> | <a href="<c:url value="/admin/modelo/remove/${c.idModelo}"/>">Remover</a></td>
                  </tr>          
                </c:forEach>
            </tbody>
        </table>
    </div>
