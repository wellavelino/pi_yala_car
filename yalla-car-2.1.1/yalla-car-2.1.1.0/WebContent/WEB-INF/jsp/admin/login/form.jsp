<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div class="loginbox">
    <div class="loginboxinner">

        <div class="logo">
            <h1><span>Yala</span>Car</h1>
            <p>Favor Realizar Login</p>
        </div><!--logo-->

        <br clear="all" /><br />

        <form id="login" method="POST" action="<c:url value="/admin/login/logon"/>" >

            <div class="username">
                    <div class="usernameinner">
                    <input type="text" name="usuario.login" id="usuario" placeholder="Usu�rio" />
                </div>
            </div>
 
            <div class="password">
                    <div class="passwordinner">
                    <input type="password" name="usuario.senha" id="senha" placeholder="Senha"  />
                </div>
            </div>

            <button>Entrar</button>
        </form>

    </div><!--loginboxinner-->
</div><!--loginbox-->