<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<div class="pageheader notab">
    <h1 class="pagetitle">Editar Concessionária</h1>
    <span class="pagedesc">formulário disponível para edição de concessionárias</span>
</div><!--pageheader-->

<div id="contentwrapper" class="contentwrapper">

    <div id="basicform" class="subcontent">

        <form class="stdform stdform2" method="POST" action="<c:url value="/admin/concessionaria/update/${concessionaria.idConcessionaria}"/>">            

            <p>
                <label>CNPJ</label>
                <span class="field"><input type="text" name="concessionaria.cnpj" class="smallinput" value="${concessionaria.cnpj}"/></span>
            </p>

            <p>
                <label>Nome</label>
                <span class="field"><input type="text" name="concessionaria.nome" class="longinput" value="${concessionaria.nome}"/></span>
            </p>

            <p>
                <label>Endereço</label>
                <span class="field"><input type="text" name="concessionaria.endereco" class="longinput" value="${concessionaria.endereco}"/></span>
            </p>

            <p>
                <label>Bairro</label>
                <span class="field"><input type="text" name="concessionaria.bairro" class="smallinput" value="${concessionaria.bairro}"/></span>
            </p>  

            <p>
                <label>Cidade</label>
                <span class="field"><input type="text" name="concessionaria.cidade" class="smallinput" value="${concessionaria.cidade}"/></span>
            </p>  

            <p>
                <label>UF</label>
                <span class="field">
                    <select name="concessionaria.uf">  
                        <c:forEach items="${comboEstados}" var="ce">  
                            <option <c:if test="${concessionaria.uf == ce[1]}"> selected="selected"</c:if>  
                                value="<c:out value="${ce[1]}" />">${ce[0]}</option>  
                        </c:forEach>  
                    </select>
                </span>
            </p> 

            <p>
                <label>Telefone Contato</label>
                <span class="field"><input type="text" name="concessionaria.telefoneContato" class="smallinput" value="${concessionaria.telefoneContato}"/></span>
            </p>  

            <p>
                <label>Fabricantes</label>
                <span class="field">
                    <select name="concessionaria.fabricante.idFabricante">  
                        <c:forEach items="${listaFabricantes}" var="f">  
                            <option <c:if test="${concessionaria.fabricante.idFabricante == f.idFabricante}"> selected="selected"</c:if>  
                                value="<c:out value="${f.idFabricante}" />">${f.nome}</option> 
                        </c:forEach>  
                    </select>
                </span>
            </p>
             
            <input type="hidden" name="_method" value="PUT"/>
            
            <p class="stdformbutton">
                <button type="submit" class="submit radius2">Atualizar</button>
                <input type="reset" class="reset radius2" value="Resetar Formulário"/>
            </p>

        </form>

        <br/>

    </div>
</div><!--contentwrapper-->