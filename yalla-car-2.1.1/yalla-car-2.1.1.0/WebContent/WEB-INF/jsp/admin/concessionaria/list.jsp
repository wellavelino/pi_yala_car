<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
              
    <div class="pageheader notab">
        <h1 class="pagetitle">Listagem de Concessionárias</h1>
        <span class="pagedesc">busca de concessionárias cadastradas</span>
    </div>

    <div id="contentwrapper" class="contentwrapper">

        <div class="contenttitle2">
            <h3>Concessionárias</h3>
        </div><!--contenttitle-->

        <!--<div class="tableoptions">

            <select class="radius3">
                <option value="">SELECIONE UM FABRICANTE</option>
                <option value="">FIAT</option>
                <option value="">HONDA</option>
                <option value="">CHEVROLET</option>
                <option value="">VOLKSWAGEM</option>
                <option value="">HYUNDAY</option>
                <option value="">FORD</option>
            </select> &nbsp;

            <button class="radius3">FILTRAR</button>
            
        </div>-->

        <table cellpadding="0" cellspacing="0" border="0" class="stdtable">
            <colgroup>
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
                <col class="con1" />
                <col class="con0" />
            </colgroup>
            <thead>
                <tr>
                    <th class="head0">CNPJ</th>
                    <th class="head1">NOME</th>
                    <th class="head0">CIDADE</th>
                    <th class="head0">TELEFONE</th>
                    <th class="head0">AÇÕES</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${concessionariaList}" var="c">
                  <tr>
                    <td>${c.cnpj}</td>
                    <td>${c.nome}</td>
                    <td>${c.cidade}</td>
                    <td>${c.telefoneContato}</td>
                    <td class="center"><a href="<c:url value="/admin/concessionaria/edit/${c.idConcessionaria}"/>">Editar</a> | <a href="<c:url value="/admin/concessionaria/remove/${c.idConcessionaria}"/>">Remover</a></td>
                  </tr>          
                </c:forEach>
            </tbody>
        </table>
    </div>
