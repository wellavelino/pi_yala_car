$(function(){
    $('body').on('click', '.content-marcas a',function(e)
    {
        e.preventDefault();
        var self = $(this);
        $.ajax({
            dataType:"html",
            url: self.attr('href'),
            success: function(data) {
                if($('.slider').get(0))
                    $('.slider').remove();
                $(".container-carousel-marcas").after(data);
                $('.slider').hide().fadeIn('slow');
            }    
        });
    });
});