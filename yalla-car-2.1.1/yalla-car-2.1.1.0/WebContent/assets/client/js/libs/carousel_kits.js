;(function(namespace, $)
{
    var Carousel = 
    {
        Class : function () 
        {
            this.doc = document;
            this.num_page;
            
            this.init = function (el,_num_page) {
                this.root = el;
                this.num_page = _num_page;
                this.currentPage = 1;
                this.stage = this.root.querySelector('.content-marcas');
                this.li = this.stage.querySelectorAll('li');
                console.log(this.li.length);
                if (this.li.length < this.num_page) {
                    return;
                }             
                this.build();
            };

            this.build = function () 
            {
                if (this.li.length < this.num_page)
                    return false;

                this.getPageDimensions()
                    .createStage()
                    .initNavigation();
            };
            this.getPageDimensions = function () 
            {
                var i;
                this.pageWidth = this.pageHeight = 0;
                for (i = 0; i < this.li.length; i += 1) {
                    if (this.li[i].clientWidth > this.pageWidth) {
                        this.pageWidth = this.li[i].clientWidth;
                    }
                    if (this.li[i].clientHeight > this.pageHeight) {
                        this.pageHeight = this.li[i].clientHeight;
                    }
                }
                return this;
            };
            this.createStage = function () 
            {
                
                this.ulList = this.stage.children;
                this.stage.style.height = (this.num_page * this.pageHeight) + 'px'; 
                this.ulList[0].style.height = (this.li.length  * this.pageHeight) + 'px';     

                return this;
            };
            this.initNavigation = function () {
                var positionTop = ((this.pageHeight / 2) - 40) + 'px';
             
                if (this.li.length < 2) {
                    return this;
                }
             
                this.createNavigationButton('seta-top', positionTop)
                    .createNavigationButton('seta-bottom', positionTop);
                          
                return this;
            };
            this.createNavigationButton = function (direction, positionTop) {
                var navButton = this.doc.createElement('a'),
                    self = this,
                    slidingBottom = (direction === 'seta-bottom'),
                    page;
             
                navButton.className = 'seta-kits ' + direction + (slidingBottom ? ' off' : '');
                navButton.href = '#';
                navButton.onclick = function (e) {
                    e.preventDefault();
                    page = (slidingBottom ? self.currentPage - self.num_page : self.currentPage + self.num_page);
                    self.gotoPage(page,self.num_page,self.ulList);
                };
                if (direction === 'seta-bottom') 
                    this.root.appendChild(navButton);
                else
                    this.root.insertBefore(navButton, this.stage);
             
                return this;
            };
            this.gotoPage = function (page,num_page,ulList) {
                var marginTop = (-1) * (((page) - 1) * this.pageHeight);
                if (page < 1 || page >= this.li.length) {
                    return;
                }
             
                this.setNavigationState(page);
                ulList[0].style.marginTop = marginTop + 'px';
                this.currentPage = page;
            };
            this.setNavigationState = function (page) {
                this.navButtonsList = this.root.querySelectorAll('.seta-kits');
                if (page === 1) {
                    this.navButtonsList[0].classList.add('off');
                    this.navButtonsList[1].classList.remove('off');
                } else {
                    this.navButtonsList[0].classList.remove('off');
                    if (page === this.li.length) {
                        this.navButtonsList[1].classList.add('off');
                    } else {
                        this.navButtonsList[1].classList.remove('off');
                    }
                }
            };
        }
    }
    function init (selector,_num_page) {
        var elements = document.querySelectorAll(selector),
            i;

        var slidersList = [];
        if (elements.length < 1) {
            return;
        }

        for (i = 0; i < elements.length; i += 1) {
            slidersList.push(new Carousel.Class().init(elements[i],_num_page));
        }
    }

    init('.container-carousel-marcas',5);
    // var slider = new JSlider('.slider-veiculo',1); 
    // var slider = new JSlider('.slider-kits',1); 
})(window, jQuery)