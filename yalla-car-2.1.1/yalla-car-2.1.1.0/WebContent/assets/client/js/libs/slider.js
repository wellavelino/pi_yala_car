;(function(namespace, $)
{
    var Slider = 
    {
        Class : function (_kits) 
        {
            this.doc = document;
            this.num_page;
            this.kits = _kits;
            this.divList;
            this.init = function (el,_num_page) {
                this.root = el;
                this.num_page = _num_page;
                this.currentPage = 1;

                if (this.kits)
                    this.li = this.root.querySelectorAll('.kits');
                else 
                    this.li = this.root.querySelectorAll('li');
                
                if (this.li.length < this.num_page) {
                    return;
                }             
                this.build();
            };

            this.build = function () 
            {
                this.getPageDimensions()
                    .createStage()
                    .initNavigation();
                this.root.innerHTML = '';
                this.root.appendChild(this.stage);
            };
            this.getPageDimensions = function () 
            {
                var i;
                this.pageWidth = this.pageHeight = 0;
                for (i = 0; i < this.li.length; i += 1) {
                    if (this.li[i].clientWidth > this.pageWidth) {
                        this.pageWidth = this.li[i].clientWidth;
                    }
                    if (this.li[i].clientHeight > this.pageHeight) {
                        this.pageHeight = this.li[i].clientHeight;
                    }
                }
                return this;
            };
            this.createStage = function () 
            {
                this.stage = this.doc.createElement('div');
                this.stage.className = 'container-carousel-veiculos';
                this.buildTrack()
                    .loadImages();         
                this.stage.appendChild(this.sliderTrack);
             
                return this;
            };
            this.buildTrack = function () 
            {
                this.sliderTrack = this.doc.createElement('div');
                this.sliderTrack.className = 'content-veiculos';
                return this;
            };
            this.loadImages = function () {
                var i,
                    li;
                this.imageList = this.doc.createElement('ul');
                this.contentLi = this.doc.createElement('div'); 
                this.contentLi.style.display ='inline-block';
                this.contentLi.className = 'content-li';

                for (i = 0; i < this.li.length; i += 1) {
                    if (i%this.num_page == 0) {
                        this.contentLi = this.doc.createElement('div'); 
                        this.contentLi.style.display ='inline-block';
                        this.contentLi.style.verticalAlign ='top';
                        this.contentLi.className = 'content-li';
                    }
                    this.li[i].style.width = this.pageWidth + 'px';
                    this.li[i].style.height = this.pageHeight + 'px';
                    this.contentLi.appendChild(this.li[i]);
                    this.imageList.appendChild(this.contentLi);
                }
                
                this.divList = this.imageList.querySelectorAll('.content-li');
                this.imageList.style.width = (this.divList.length  * this.pageWidth) + 'px';
                this.sliderTrack.appendChild(this.imageList);
            };
            this.initNavigation = function () {
                var positionTop = ((this.pageHeight / 2) - 40) + 'px';
             
                if (this.li.length < 2) {
                    return this;
                }
                if(this.root.className == 'slider-veiculo' || this.root.className == 'slider-kits')
                {
                    this.calcularValor(this.divList[0]);
                    this.selectModeloCor(this.divList[0]);
                    $(this.divList[0]).addClass("current-veiculo");
                }    
                
                this.createNavigationButton('seta-left', positionTop)
                    .createNavigationButton('seta-right', positionTop);
             
                this.navButtonsList = this.stage.querySelectorAll('.seta');
             
                return this;
            };
            this.createNavigationButton = function (direction, positionTop) {
                var navButton = this.doc.createElement('a'),
                    self = this,
                    slidingLeft = (direction === 'seta-left'),
                    page;
             
                navButton.className = 'seta ' + direction + (slidingLeft ? ' off' : '');
                navButton.style.top = positionTop;
                navButton.href = '#';
                navButton.onclick = function (e) {
                    e.preventDefault();
                    page = (slidingLeft ? self.currentPage - 1 : self.currentPage + 1);
                    self.gotoPage(page,self.num_page);
                };
                if (direction === 'seta-left') 
                    this.stage.insertBefore(navButton, this.sliderTrack);
                else
                    this.stage.appendChild(navButton);
             
                return this;
            };
            this.gotoPage = function (page,num_page) {
                var marginLeft = (-1) * (((page) - 1) * this.pageWidth);
                if (page < 1 || page > this.divList.length) {
                    return;
                }
                this.setNavigationState(page);
                
                if(this.root.className == 'slider-veiculo' || this.root.className == 'slider-kits')
                    this.selectModeloCor(this.divList[page -1]);
                this.imageList.style.marginLeft = marginLeft + 'px';
                $(this.divList).removeClass("current-veiculo");
                $(this.divList[page -1]).addClass("current-veiculo");
                this.currentPage = page;
            };
            this.setNavigationState = function (page) {
                if (page === 1) {
                    this.navButtonsList[0].classList.add('off');
                    this.navButtonsList[1].classList.remove('off');
                } else {
                    this.navButtonsList[0].classList.remove('off');
                    if (page === this.li.length) {
                        this.navButtonsList[1].classList.add('off');
                    } else {
                        this.navButtonsList[1].classList.remove('off');
                    }
                }
            };
            this.selectModeloCor = function (_content)
            {
                var input;
                this.input = this.doc.createElement('input');
                this.input.type = 'hidden';
                if(_content.querySelector('li').className == '')
                    this.input.name = 'ModeloCores.idModeloCor'
                else
                    this.input.name = _content.querySelector('li').className +'.idKit';
                this.input.className = 'idMc';
                this.input.value = _content.querySelector('.id').innerHTML;
                $('.idMc').remove();
                _content.appendChild(this.input);
            };
            this.calcularValor = function (_content) 
            {
//                self = $(_content);
//                var valueVeiculo = $('.slider-veiculo .current-veiculo .valueVeiculo').val();
//                console.log(valueVeiculo);
                
            }
        }
    }
    function init (selector,_num_page, _kits) {
        var elements = document.querySelectorAll(selector),
            i;
        var slidersList = [];
        if (elements.length < 1) {
            return;
        }

        for (i = 0; i < elements.length; i += 1) {
            slidersList.push(new Slider.Class(_kits).init(elements[i],_num_page));
        }
    }
    init('.slider',8, false);
    init('.slider-veiculo',1,false);
    init('.slider-kits',1,true);
})(window, jQuery)

