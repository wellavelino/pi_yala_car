package br.com.yalla.enums;

public enum Perfil {
    
    ADMINISTRADOR, BACKOFFICE, CONCESSIONARIA;
    
}
