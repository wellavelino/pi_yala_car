/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import br.com.yalla.interfaces.ImageInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "acessorio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acessorio.findAll", query = "SELECT a FROM Acessorio a"),
    @NamedQuery(name = "Acessorio.findByIdAcessorio", query = "SELECT a FROM Acessorio a WHERE a.idAcessorio = :idAcessorio"),
    @NamedQuery(name = "Acessorio.findByNome", query = "SELECT a FROM Acessorio a WHERE a.nome = :nome"),
    @NamedQuery(name = "Acessorio.findByCategoria", query = "SELECT a FROM Acessorio a WHERE a.categoria = :categoria"),
    @NamedQuery(name = "Acessorio.findByPrecoUnitario", query = "SELECT a FROM Acessorio a WHERE a.precoUnitario = :precoUnitario"),
    @NamedQuery(name = "Acessorio.findByImagem", query = "SELECT a FROM Acessorio a WHERE a.imagem = :imagem")})
public class Acessorio implements Serializable, ImageInterface {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idAcessorio")
    private Long idAcessorio;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "categoria")
    private String categoria;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoUnitario")
    private BigDecimal precoUnitario;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "imagem")
    private String imagem;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "acessorio")
    private Collection<AcessorioKit> acessorioKitsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAcessorio")
    private Collection<AcessorioModelos> acessorioModelosCollection;
    
    @OneToMany(mappedBy = "idAcessorio")
    private Collection<PedidosAcessorios> pedidosAcessoriosCollection;

    public Acessorio() {
    }

    public Acessorio(Long idAcessorio) {
        this.idAcessorio = idAcessorio;
    }

    public Acessorio(Long idAcessorio, String nome, String categoria, BigDecimal precoUnitario, String imagem) {
        this.idAcessorio = idAcessorio;
        this.nome = nome;
        this.categoria = categoria;
        this.precoUnitario = precoUnitario;
        this.imagem = imagem;
    }

    public Long getId() {
        return this.getIdAcessorio();
    }

    public Long getIdAcessorio() {
        return idAcessorio;
    }

    public void setIdAcessorio(Long idAcessorio) {
        this.idAcessorio = idAcessorio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public BigDecimal getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(BigDecimal precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    @XmlTransient
    public Collection<AcessorioKit> getAcessorioKitsCollection() {
        return acessorioKitsCollection;
    }

    public void setAcessorioKitsCollection(Collection<AcessorioKit> acessorioKitsCollection) {
        this.acessorioKitsCollection = acessorioKitsCollection;
    }

    @XmlTransient
    public Collection<AcessorioModelos> getAcessorioModelosCollection() {
        return acessorioModelosCollection;
    }

    public void setAcessorioModelosCollection(Collection<AcessorioModelos> acessorioModelosCollection) {
        this.acessorioModelosCollection = acessorioModelosCollection;
    }

    @XmlTransient
    public Collection<PedidosAcessorios> getPedidosAcessoriosCollection() {
        return pedidosAcessoriosCollection;
    }

    public void setPedidosAcessoriosCollection(Collection<PedidosAcessorios> pedidosAcessoriosCollection) {
        this.pedidosAcessoriosCollection = pedidosAcessoriosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAcessorio != null ? idAcessorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acessorio)) {
            return false;
        }
        Acessorio other = (Acessorio) object;
        if ((this.idAcessorio == null && other.idAcessorio != null) || (this.idAcessorio != null && !this.idAcessorio.equals(other.idAcessorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Acessorio[ idAcessorio=" + idAcessorio + " ]";
    }
    
}
