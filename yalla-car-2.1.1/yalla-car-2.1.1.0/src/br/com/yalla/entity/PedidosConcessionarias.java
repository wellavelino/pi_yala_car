/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "pedidosConcessionarias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidosConcessionarias.findAll", query = "SELECT p FROM PedidosConcessionarias p"),
    @NamedQuery(name = "PedidosConcessionarias.findByIdPedidoConcessionaria", query = "SELECT p FROM PedidosConcessionarias p WHERE p.idPedidoConcessionaria = :idPedidoConcessionaria")})
public class PedidosConcessionarias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idPedidoConcessionaria")
    private Long idPedidoConcessionaria;
    @JoinColumn(name = "idConcessionaria", referencedColumnName = "idConcessionaria")
    @ManyToOne(optional = false)
    private Concessionaria idConcessionaria;
    @JoinColumn(name = "idPedido", referencedColumnName = "idPedido")
    @ManyToOne(optional = false)
    private Pedidos idPedido;

    public PedidosConcessionarias() {
    }

    public PedidosConcessionarias(Long idPedidoConcessionaria) {
        this.idPedidoConcessionaria = idPedidoConcessionaria;
    }

    public Long getIdPedidoConcessionaria() {
        return idPedidoConcessionaria;
    }

    public void setIdPedidoConcessionaria(Long idPedidoConcessionaria) {
        this.idPedidoConcessionaria = idPedidoConcessionaria;
    }

    public Concessionaria getIdConcessionaria() {
        return idConcessionaria;
    }

    public void setIdConcessionaria(Concessionaria idConcessionaria) {
        this.idConcessionaria = idConcessionaria;
    }

    public Pedidos getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Pedidos idPedido) {
        this.idPedido = idPedido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedidoConcessionaria != null ? idPedidoConcessionaria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidosConcessionarias)) {
            return false;
        }
        PedidosConcessionarias other = (PedidosConcessionarias) object;
        if ((this.idPedidoConcessionaria == null && other.idPedidoConcessionaria != null) || (this.idPedidoConcessionaria != null && !this.idPedidoConcessionaria.equals(other.idPedidoConcessionaria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.PedidosConcessionarias[ idPedidoConcessionaria=" + idPedidoConcessionaria + " ]";
    }
    
}
