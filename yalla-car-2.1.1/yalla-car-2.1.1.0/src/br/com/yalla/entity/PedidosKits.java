/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "pedidosKits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidosKits.findAll", query = "SELECT p FROM PedidosKits p"),
    @NamedQuery(name = "PedidosKits.findByIdPedidoKit", query = "SELECT p FROM PedidosKits p WHERE p.idPedidoKit = :idPedidoKit")})
public class PedidosKits implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPedidoKit")
    private Integer idPedidoKit;
    @JoinColumn(name = "idKit", referencedColumnName = "idKit")
    @ManyToOne
    private Kit idKit;
    @JoinColumn(name = "idPedido", referencedColumnName = "idPedido")
    @ManyToOne
    private Pedidos idPedido;

    public PedidosKits() {
    }

    public PedidosKits(Integer idPedidoKit) {
        this.idPedidoKit = idPedidoKit;
    }

    public Integer getIdPedidoKit() {
        return idPedidoKit;
    }

    public void setIdPedidoKit(Integer idPedidoKit) {
        this.idPedidoKit = idPedidoKit;
    }

    public Kit getIdKit() {
        return idKit;
    }

    public void setIdKit(Kit idKit) {
        this.idKit = idKit;
    }

    public Pedidos getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Pedidos idPedido) {
        this.idPedido = idPedido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedidoKit != null ? idPedidoKit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidosKits)) {
            return false;
        }
        PedidosKits other = (PedidosKits) object;
        if ((this.idPedidoKit == null && other.idPedidoKit != null) || (this.idPedidoKit != null && !this.idPedidoKit.equals(other.idPedidoKit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.PedidosKits[ idPedidoKit=" + idPedidoKit + " ]";
    }
    
}
