/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "pedidos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pedidos.findAll", query = "SELECT p FROM Pedidos p"),
    @NamedQuery(name = "Pedidos.findByIdPedido", query = "SELECT p FROM Pedidos p WHERE p.idPedido = :idPedido"),
    @NamedQuery(name = "Pedidos.findByPreco", query = "SELECT p FROM Pedidos p WHERE p.preco = :preco"),
    @NamedQuery(name = "Pedidos.findByData", query = "SELECT p FROM Pedidos p WHERE p.data = :data")})
public class Pedidos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idPedido")
    private Integer idPedido;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "preco")
    private BigDecimal preco;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPedido")
    private Collection<PedidosConcessionarias> pedidosConcessionariasCollection;
    @OneToMany(mappedBy = "idPedido")
    private Collection<PedidosKits> pedidosKitsCollection;
    @JoinColumn(name = "idCliente", referencedColumnName = "idCliente")
    @ManyToOne(optional = false)
    private Clientes idCliente;
    @JoinColumn(name = "idModeloCor", referencedColumnName = "idModeloCor")
    @ManyToOne(optional = false)
    private ModeloCor idModeloCor;
    @JoinColumn(name = "idModelo", referencedColumnName = "idModelo")
    @ManyToOne(optional = false)
    private Modelo idModelo;
    @OneToMany(mappedBy = "idPedido")
    private Collection<PedidosAcessorios> pedidosAcessoriosCollection;

    public Pedidos() {
    }

    public Pedidos(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public Pedidos(Integer idPedido, BigDecimal preco, Date data) {
        this.idPedido = idPedido;
        this.preco = preco;
        this.data = data;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @XmlTransient
    public Collection<PedidosConcessionarias> getPedidosConcessionariasCollection() {
        return pedidosConcessionariasCollection;
    }

    public void setPedidosConcessionariasCollection(Collection<PedidosConcessionarias> pedidosConcessionariasCollection) {
        this.pedidosConcessionariasCollection = pedidosConcessionariasCollection;
    }

    @XmlTransient
    public Collection<PedidosKits> getPedidosKitsCollection() {
        return pedidosKitsCollection;
    }

    public void setPedidosKitsCollection(Collection<PedidosKits> pedidosKitsCollection) {
        this.pedidosKitsCollection = pedidosKitsCollection;
    }

    public Clientes getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Clientes idCliente) {
        this.idCliente = idCliente;
    }

    public ModeloCor getIdModeloCor() {
        return idModeloCor;
    }

    public void setIdModeloCor(ModeloCor idModeloCor) {
        this.idModeloCor = idModeloCor;
    }

    public Modelo getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Modelo idModelo) {
        this.idModelo = idModelo;
    }

    @XmlTransient
    public Collection<PedidosAcessorios> getPedidosAcessoriosCollection() {
        return pedidosAcessoriosCollection;
    }

    public void setPedidosAcessoriosCollection(Collection<PedidosAcessorios> pedidosAcessoriosCollection) {
        this.pedidosAcessoriosCollection = pedidosAcessoriosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedido != null ? idPedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedidos)) {
            return false;
        }
        Pedidos other = (Pedidos) object;
        if ((this.idPedido == null && other.idPedido != null) || (this.idPedido != null && !this.idPedido.equals(other.idPedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Pedidos[ idPedido=" + idPedido + " ]";
    }
    
}
