package br.com.yalla.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "veiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Veiculo.findAll", query = "SELECT v FROM Veiculo v"),
    @NamedQuery(name = "Veiculo.findByIdVeiculo", query = "SELECT v FROM Veiculo v WHERE v.idVeiculo = :idVeiculo"),
    @NamedQuery(name = "Veiculo.findByNome", query = "SELECT v FROM Veiculo v WHERE v.nome = :nome"),
    @NamedQuery(name = "Veiculo.findByCategoria", query = "SELECT v FROM Veiculo v WHERE v.categoria = :categoria"),
    @NamedQuery(name = "Veiculo.findByQuantidadePorta", query = "SELECT v FROM Veiculo v WHERE v.quantidadePorta = :quantidadePorta")})
public class Veiculo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idVeiculo")
    private Long idVeiculo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "categoria")
    private String categoria;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantidadePorta")
    private Integer quantidadePorta;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "veiculo") 
    private Collection<Modelo> modelosCollection;
    
    @JoinColumn(name = "idFabricante", referencedColumnName = "idFabricante")
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
    private Fabricante fabricante;

    public Veiculo() {
    }

    public Veiculo(Long idVeiculo) {
        this.idVeiculo = idVeiculo;
    }

    public Veiculo(Long idVeiculo, String nome, String categoria, int quantidadePorta) {
        this.idVeiculo = idVeiculo;
        this.nome = nome;
        this.categoria = categoria;
        this.quantidadePorta = quantidadePorta;
    }
    
    public Long getId() {
        return this.getIdVeiculo();
    }

    public Long getIdVeiculo() {
        return idVeiculo;
    }

    public void setIdVeiculo(Long idVeiculo) {
        this.idVeiculo = idVeiculo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getQuantidadePorta() {
        return quantidadePorta;
    }

    public void setQuantidadePorta(Integer quantidadePorta) {
        this.quantidadePorta = quantidadePorta;
    }

    @XmlTransient
    public Collection<Modelo> getModelosCollection() {
        return modelosCollection;
    }

    public void setModelosCollection(Collection<Modelo> modelosCollection) {
        this.modelosCollection = modelosCollection;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVeiculo != null ? idVeiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Veiculo)) {
            return false;
        }
        Veiculo other = (Veiculo) object;
        if ((this.idVeiculo == null && other.idVeiculo != null) || (this.idVeiculo != null && !this.idVeiculo.equals(other.idVeiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Veiculos[ idVeiculo=" + idVeiculo + " ]";
    }    
}
