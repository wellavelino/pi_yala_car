package br.com.yalla.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "kits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kit.findAll", query = "SELECT k FROM Kit k"),
    @NamedQuery(name = "Kit.findByIdKit", query = "SELECT k FROM Kit k WHERE k.idKit = :idKit"),
    @NamedQuery(name = "Kit.findByNome", query = "SELECT k FROM Kit k WHERE k.nome = :nome"),
    @NamedQuery(name = "Kit.findByPrecoTotal", query = "SELECT k FROM Kit k WHERE k.precoTotal = :precoTotal"),
    @NamedQuery(name = "Kit.findByDesconto", query = "SELECT k FROM Kit k WHERE k.desconto = :desconto"),
    @NamedQuery(name = "Kit.findByBasePreco", query = "SELECT k FROM Kit k WHERE k.basePreco = :basePreco")
})
public class Kit implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idKit")
    private Long idKit;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nome")
    private String nome;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoTotal")
    private BigDecimal precoTotal;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "desconto")
    private BigDecimal desconto;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "basePreco")
    private BigDecimal basePreco;
    
    @OneToMany(mappedBy = "idKit")
    private List<PedidosKits> pedidosKitsCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kit")
    private List<AcessorioKit> acessorioKitsCollection;

    public Kit() {
    }

    public Kit(Long idKit) {
        this.idKit = idKit;
    }

    public Kit(Long idKit, String nome, BigDecimal precoTotal, BigDecimal desconto, BigDecimal basePreco) {
        this.idKit = idKit;
        this.nome = nome;
        this.precoTotal = precoTotal;
        this.desconto = desconto;
        this.basePreco = basePreco;
    }

    public List<PedidosKits> getPedidosKitsCollection() {
        return pedidosKitsCollection;
    }

    public void setPedidosKitsCollection(List<PedidosKits> pedidosKitsCollection) {
        this.pedidosKitsCollection = pedidosKitsCollection;
    }

    public List<AcessorioKit> getAcessorioKitsCollection() {
        return acessorioKitsCollection;
    }

    public void setAcessorioKitsCollection(List<AcessorioKit> acessorioKitsCollection) {
        this.acessorioKitsCollection = acessorioKitsCollection;
    }

    public Long getId() {
        return this.getIdKit();
    }

    public Long getIdKit() {
        return idKit;
    }

    public void setIdKit(Long idKit) {
        this.idKit = idKit;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(BigDecimal precoTotal) {
        this.precoTotal = precoTotal;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public BigDecimal getBasePreco() {
        return basePreco;
    }

    public void setBasePreco(BigDecimal basePreco) {
        this.basePreco = basePreco;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKit != null ? idKit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kit)) {
            return false;
        }
        Kit other = (Kit) object;
        if ((this.idKit == null && other.idKit != null) || (this.idKit != null && !this.idKit.equals(other.idKit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Kits[ idKit=" + idKit + " ]";
    }
    
}
