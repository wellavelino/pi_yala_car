/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "acessorioModelos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcessorioModelos.findAll", query = "SELECT a FROM AcessorioModelos a"),
    @NamedQuery(name = "AcessorioModelos.findByIdAcessorioModelo", query = "SELECT a FROM AcessorioModelos a WHERE a.idAcessorioModelo = :idAcessorioModelo")})
public class AcessorioModelos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idAcessorioModelo")
    private Integer idAcessorioModelo;
    @JoinColumn(name = "idAcessorio", referencedColumnName = "idAcessorio")
    @ManyToOne(optional = false)
    private Acessorio idAcessorio;
    @JoinColumn(name = "idModelo", referencedColumnName = "idModelo")
    @ManyToOne(optional = false)
    private Modelo idModelo;

    public AcessorioModelos() {
    }

    public AcessorioModelos(Integer idAcessorioModelo) {
        this.idAcessorioModelo = idAcessorioModelo;
    }

    public Integer getIdAcessorioModelo() {
        return idAcessorioModelo;
    }

    public void setIdAcessorioModelo(Integer idAcessorioModelo) {
        this.idAcessorioModelo = idAcessorioModelo;
    }

    public Acessorio getIdAcessorio() {
        return idAcessorio;
    }

    public void setIdAcessorio(Acessorio idAcessorio) {
        this.idAcessorio = idAcessorio;
    }

    public Modelo getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Modelo idModelo) {
        this.idModelo = idModelo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAcessorioModelo != null ? idAcessorioModelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcessorioModelos)) {
            return false;
        }
        AcessorioModelos other = (AcessorioModelos) object;
        if ((this.idAcessorioModelo == null && other.idAcessorioModelo != null) || (this.idAcessorioModelo != null && !this.idAcessorioModelo.equals(other.idAcessorioModelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.AcessorioModelos[ idAcessorioModelo=" + idAcessorioModelo + " ]";
    }
    
}
