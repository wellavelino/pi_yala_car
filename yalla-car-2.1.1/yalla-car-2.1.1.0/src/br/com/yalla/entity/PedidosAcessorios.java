/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "pedidosAcessorios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidosAcessorios.findAll", query = "SELECT p FROM PedidosAcessorios p"),
    @NamedQuery(name = "PedidosAcessorios.findByIdPedidoAcessorio", query = "SELECT p FROM PedidosAcessorios p WHERE p.idPedidoAcessorio = :idPedidoAcessorio")})
public class PedidosAcessorios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idPedidoAcessorio")
    private Integer idPedidoAcessorio;
    @JoinColumn(name = "idAcessorio", referencedColumnName = "idAcessorio")
    @ManyToOne
    private Acessorio idAcessorio;
    @JoinColumn(name = "idPedido", referencedColumnName = "idPedido")
    @ManyToOne
    private Pedidos idPedido;

    public PedidosAcessorios() {
    }

    public PedidosAcessorios(Integer idPedidoAcessorio) {
        this.idPedidoAcessorio = idPedidoAcessorio;
    }

    public Integer getIdPedidoAcessorio() {
        return idPedidoAcessorio;
    }

    public void setIdPedidoAcessorio(Integer idPedidoAcessorio) {
        this.idPedidoAcessorio = idPedidoAcessorio;
    }

    public Acessorio getIdAcessorio() {
        return idAcessorio;
    }

    public void setIdAcessorio(Acessorio idAcessorio) {
        this.idAcessorio = idAcessorio;
    }

    public Pedidos getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Pedidos idPedido) {
        this.idPedido = idPedido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedidoAcessorio != null ? idPedidoAcessorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidosAcessorios)) {
            return false;
        }
        PedidosAcessorios other = (PedidosAcessorios) object;
        if ((this.idPedidoAcessorio == null && other.idPedidoAcessorio != null) || (this.idPedidoAcessorio != null && !this.idPedidoAcessorio.equals(other.idPedidoAcessorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.PedidosAcessorios[ idPedidoAcessorio=" + idPedidoAcessorio + " ]";
    }
    
}
