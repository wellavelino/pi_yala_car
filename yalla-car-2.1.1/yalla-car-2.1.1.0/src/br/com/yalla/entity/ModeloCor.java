package br.com.yalla.entity;

import br.com.yalla.interfaces.ImageInterface;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "modeloCores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModeloCor.findAll", query = "SELECT m FROM ModeloCor m"),
    @NamedQuery(name = "ModeloCor.findByIdModelo", query = "SELECT m FROM ModeloCor m WHERE m.modelo = :idModelo"),
    @NamedQuery(name = "ModeloCor.findByIdModeloCor", query = "SELECT m FROM ModeloCor m WHERE m.idModeloCor = :idModeloCor"),
    @NamedQuery(name = "ModeloCor.findByNome", query = "SELECT m FROM ModeloCor m WHERE m.nome = :nome"),
    @NamedQuery(name = "ModeloCor.findByImagem", query = "SELECT m FROM ModeloCor m WHERE m.imagem = :imagem")})

public class ModeloCor implements Serializable, ImageInterface {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idModeloCor")
    private Long idModeloCor;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "imagem")
    private String imagem;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idModeloCor")
    private Collection<Pedidos> pedidosCollection;
    
    @JoinColumn(name = "idModelo", referencedColumnName = "idModelo")
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
    private Modelo modelo;

    public ModeloCor() {
    }

    public ModeloCor(Long idModeloCor) {
        this.idModeloCor = idModeloCor;
    }

    public ModeloCor(Long idModeloCor, String nome, String imagem) {
        this.idModeloCor = idModeloCor;
        this.nome = nome;
        this.imagem = imagem;
    }
    
     public Long getId() {
        return this.getIdModeloCor();
    }

    public Long getIdModeloCor() {
        return idModeloCor;
    }

    public void setIdModeloCor(Long idModeloCor) {
        this.idModeloCor = idModeloCor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    @XmlTransient
    public Collection<Pedidos> getPedidosCollection() {
        return pedidosCollection;
    }

    public void setPedidosCollection(Collection<Pedidos> pedidosCollection) {
        this.pedidosCollection = pedidosCollection;
    }

    public Modelo getIdModelo() {
        return modelo;
    }

    public void setIdModelo(Modelo idModelo) {
        this.modelo = idModelo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModeloCor != null ? idModeloCor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeloCor)) {
            return false;
        }
        ModeloCor other = (ModeloCor) object;
        if ((this.idModeloCor == null && other.idModeloCor != null) || (this.idModeloCor != null && !this.idModeloCor.equals(other.idModeloCor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.ModeloCores[ idModeloCor=" + idModeloCor + " ]";
    }
    
    
    
}
