/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "concessionarias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Concessionaria.findAll", query = "SELECT c FROM Concessionaria c"),
    @NamedQuery(name = "Concessionaria.findByIdConcessionaria", query = "SELECT c FROM Concessionaria c WHERE c.idConcessionaria = :idConcessionaria"),
    @NamedQuery(name = "Concessionaria.findByCnpj", query = "SELECT c FROM Concessionaria c WHERE c.cnpj = :cnpj"),
    @NamedQuery(name = "Concessionaria.findByNome", query = "SELECT c FROM Concessionaria c WHERE c.nome = :nome"),
    @NamedQuery(name = "Concessionaria.findByEndereco", query = "SELECT c FROM Concessionaria c WHERE c.endereco = :endereco"),
    @NamedQuery(name = "Concessionaria.findByBairro", query = "SELECT c FROM Concessionaria c WHERE c.bairro = :bairro"),
    @NamedQuery(name = "Concessionaria.findByCidade", query = "SELECT c FROM Concessionaria c WHERE c.cidade = :cidade"),
    @NamedQuery(name = "Concessionaria.findByUf", query = "SELECT c FROM Concessionaria c WHERE c.uf = :uf"),
    @NamedQuery(name = "Concessionaria.findByTelefoneContato", query = "SELECT c FROM Concessionaria c WHERE c.telefoneContato = :telefoneContato")})
public class Concessionaria implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idConcessionaria")
    private Long idConcessionaria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 18)
    @Column(name = "cnpj")
    private String cnpj;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "endereco")
    private String endereco;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "bairro")
    private String bairro;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "cidade")    
    private String cidade;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "uf")
    private String uf;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefoneContato")
    private String telefoneContato;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConcessionaria")
    private Collection<PedidosConcessionarias> pedidosConcessionariasCollection;
    
    @JoinColumn(name = "idFabricante", referencedColumnName = "idFabricante")
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
    private Fabricante fabricante;

    public Concessionaria() {
    }

    public Concessionaria(Long idConcessionaria) {
        this.idConcessionaria = idConcessionaria;
    }

    public Concessionaria(Long idConcessionaria, String cnpj, String nome, String endereco, String bairro, String cidade, String uf, String telefoneContato) {
        this.idConcessionaria = idConcessionaria;
        this.cnpj = cnpj;
        this.nome = nome;
        this.endereco = endereco;
        this.bairro = bairro;
        this.cidade = cidade;
        this.uf = uf;
        this.telefoneContato = telefoneContato;
    }

    public Long getId() {
        return this.getIdConcessionaria();
    }

    public Long getIdConcessionaria() {
        return idConcessionaria;
    }

    public void setIdConcessionaria(Long idConcessionaria) {
        this.idConcessionaria = idConcessionaria;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getTelefoneContato() {
        return telefoneContato;
    }

    public void setTelefoneContato(String telefoneContato) {
        this.telefoneContato = telefoneContato;
    }

    @XmlTransient
    public Collection<PedidosConcessionarias> getPedidosConcessionariasCollection() {
        return pedidosConcessionariasCollection;
    }

    public void setPedidosConcessionariasCollection(Collection<PedidosConcessionarias> pedidosConcessionariasCollection) {
        this.pedidosConcessionariasCollection = pedidosConcessionariasCollection;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConcessionaria != null ? idConcessionaria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Concessionaria)) {
            return false;
        }
        Concessionaria other = (Concessionaria) object;
        if ((this.idConcessionaria == null && other.idConcessionaria != null) || (this.idConcessionaria != null && !this.idConcessionaria.equals(other.idConcessionaria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Concessionarias[ idConcessionaria=" + idConcessionaria + " ]";
    }
    
}
