package br.com.yalla.entity;

import br.com.yalla.interfaces.ImageInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "modelos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modelo.findAll", query = "SELECT m FROM Modelo m"),
    @NamedQuery(name = "Modelo.findByIdModelo", query = "SELECT m FROM Modelo m WHERE m.idModelo = :idModelo"),    
    //@NamedQuery(name = "Modelo.findByIdVeiculo", query = "SELECT m FROM Modelo m WHERE m.veiculo = :idVeiculo"),
    @NamedQuery(name = "Modelo.findByNome", query = "SELECT m FROM Modelo m WHERE m.nome = :nome"),
    @NamedQuery(name = "Modelo.findByPreco", query = "SELECT m FROM Modelo m WHERE m.preco = :preco"),
    @NamedQuery(name = "Modelo.findByAnoFabricacao", query = "SELECT m FROM Modelo m WHERE m.anoFabricacao = :anoFabricacao")})
public class Modelo implements Serializable, ImageInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idModelo")
    private Long idModelo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nome")
    private String nome;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "preco")
    private BigDecimal preco;
    
    @Size(max = 50)
    @Column(name = "thumb")
    private String thumb;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "anoFabricacao")    
    private String anoFabricacao;    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idModelo")
    private Collection<Pedidos> pedidosCollection;
    
    @JoinColumn(name = "idVeiculo", referencedColumnName = "idVeiculo")
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
    private Veiculo veiculo; 
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idModelo")    
    private Collection<AcessorioModelos> acessorioModelosCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modelo")    
    private Collection<ModeloCor> modeloCoresCollection;

    public Modelo() {
    }

    public Modelo(Long idModelo) {
        this.idModelo = idModelo;
    }

    public Modelo(Long idModelo, String nome, BigDecimal preco, String anoFabricacao) {
        this.idModelo = idModelo;
        this.nome = nome;
        this.preco = preco;
        this.anoFabricacao = anoFabricacao;
    }

     public Long getId() {
        return this.getIdModelo();
    }

    
    public Long getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(Long idModelo) {
        this.idModelo = idModelo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    @XmlTransient
    public Collection<Pedidos> getPedidosCollection() {
        return pedidosCollection;
    }

    public void setPedidosCollection(Collection<Pedidos> pedidosCollection) {
        this.pedidosCollection = pedidosCollection;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo Veiculo) {
        this.veiculo = Veiculo;
    }

    @XmlTransient
    public Collection<AcessorioModelos> getAcessorioModelosCollection() {
        return acessorioModelosCollection;
    }

    public void setAcessorioModelosCollection(Collection<AcessorioModelos> acessorioModelosCollection) {
        this.acessorioModelosCollection = acessorioModelosCollection;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
    
    @XmlTransient
    public Collection<ModeloCor> getModeloCoresCollection() {
        return modeloCoresCollection;
    }

    public void setModeloCoresCollection(Collection<ModeloCor> modeloCoresCollection) {
        this.modeloCoresCollection = modeloCoresCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModelo != null ? idModelo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modelo)) {
            return false;
        }
        Modelo other = (Modelo) object;
        if ((this.idModelo == null && other.idModelo != null) || (this.idModelo != null && !this.idModelo.equals(other.idModelo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Modelos[ idModelo=" + idModelo + " ]";
    }

    @Override
    public String getImagem() {
        return getThumb();
    }
    
}
