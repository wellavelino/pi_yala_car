package br.com.yalla.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "acessorioKits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcessorioKit.findAll", query = "SELECT a FROM AcessorioKit a"),    
    @NamedQuery(name = "AcessorioKit.findAllKit", query = "SELECT a.kit FROM AcessorioKit a"),    
//    @NamedQuery(name = "AcessorioKits.findAllAcessorio", query = "SELECT Acessorio FROM AcessorioKits a"),
    @NamedQuery(name = "AcessorioKit.findByIdAcessorioKit", query = "SELECT a FROM AcessorioKit a WHERE a.idAcessorioKit = :idAcessorioKit"),
//    @NamedQuery(name = "AcessorioKits.findByIdAcessorioKit", query = "SELECT k FROM AcessorioKits a WHERE a.idAcessorioKit = :idAcessorioKit"),

})
public class AcessorioKit implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdAcessorioKit")
    private Long idAcessorioKit;
    
    @JoinColumn(name = "idAcessorio", referencedColumnName = "idAcessorio")
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
    private Acessorio acessorio;
    
    @JoinColumn(name = "idKit", referencedColumnName = "idKit")
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false)
    private Kit kit;

    public AcessorioKit() {
    }

    public AcessorioKit(Long idAcessorioKit) {
        this.idAcessorioKit = idAcessorioKit;
    }

    public Long getId() {
        return this.getIdAcessorioKit();
    }

    public Long getIdAcessorioKit() {
        return idAcessorioKit;
    }

    public void setIdAcessorioKit(Long idAcessorioKit) {
        this.idAcessorioKit = idAcessorioKit;
    }

    public Acessorio getAcessorio() {
        return acessorio;
    }

    public void setAcessorio(Acessorio idAcessorio) {
        this.acessorio = idAcessorio;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAcessorioKit != null ? idAcessorioKit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcessorioKit)) {
            return false;
        }
        AcessorioKit other = (AcessorioKit) object;
        if ((this.idAcessorioKit == null && other.idAcessorioKit != null) || (this.idAcessorioKit != null && !this.idAcessorioKit.equals(other.idAcessorioKit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.AcessorioKits[ idAcessorioKit=" + idAcessorioKit + " ]";
    }
    
}
