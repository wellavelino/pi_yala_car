/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.entity;

import br.com.yalla.interfaces.ImageInterface;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marlon Sagrilo
 */
@Entity
@Table(name = "fabricantes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fabricante.findAll", query = "SELECT f FROM Fabricante f"),
    @NamedQuery(name = "Fabricante.findByIdFabricante", query = "SELECT f FROM Fabricante f WHERE f.idFabricante = :idFabricante"),
    @NamedQuery(name = "Fabricante.findByNome", query = "SELECT f FROM Fabricante f WHERE f.nome = :nome"),
    @NamedQuery(name = "Fabricante.findByImagem", query = "SELECT f FROM Fabricante f WHERE f.imagem = :imagem")})
public class Fabricante implements Serializable, ImageInterface {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "idFabricante")
    private Long idFabricante;
    
    @Size(max = 25)
    @Column(name = "nome")
    private String nome;
    
    @Size(max = 255)
    @Column(name = "imagem")
    private String imagem;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fabricante")
    private Collection<Concessionaria> concessionariasCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fabricante")
    private Collection<Veiculo> veiculosCollection;

    public Fabricante() {
    }

    public Fabricante(Long idFabricante) {
        this.idFabricante = idFabricante;
    }

    public Long getId() {
        return this.getIdFabricante();
    }

    public Long getIdFabricante() {
        return idFabricante;
    }

    public void setIdFabricante(Long idFabricante) {
        this.idFabricante = idFabricante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    @XmlTransient
    public Collection<Concessionaria> getConcessionariasCollection() {
        return concessionariasCollection;
    }

    public void setConcessionariasCollection(Collection<Concessionaria> concessionariasCollection) {
        this.concessionariasCollection = concessionariasCollection;
    }

    @XmlTransient
    public Collection<Veiculo> getVeiculosCollection() {
        return veiculosCollection;
    }

    public void setVeiculosCollection(Collection<Veiculo> veiculosCollection) {
        this.veiculosCollection = veiculosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFabricante != null ? idFabricante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fabricante)) {
            return false;
        }
        Fabricante other = (Fabricante) object;
        if ((this.idFabricante == null && other.idFabricante != null) || (this.idFabricante != null && !this.idFabricante.equals(other.idFabricante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.yalla.entity.Fabricantes[ idFabricante=" + idFabricante + " ]";
    }
    
}
