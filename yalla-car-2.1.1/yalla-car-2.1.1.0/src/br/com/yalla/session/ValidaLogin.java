/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.session;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.yalla.entity.Pessoa; 
import br.com.yalla.entity.Usuario;

/**
 *
 * @author Marlon Sagrilo
 */
@Component
@SessionScoped
public class ValidaLogin { 

    private Pessoa logado;

    public void login(Pessoa pessoa) {
        this.logado = pessoa;
    }
    
    public Usuario getUser() {
        
        Usuario _user = new Usuario();
        for (Usuario user: this.logado.getUsuariosCollection()) {        
            _user = user;
            break;
        }
        return _user;
    } 
    
    public String getPerfil() {        
        return this.getUser().getPerfil().toString();
    } 
    
    public String getUserID() {        
        return this.getUser().getId().toString();
    }            

    public String getNome() {
        return logado.getNome();
    }

    public String getEmail() {
        return logado.getEmail();
    }

    public boolean isLogado() {
        return logado != null;
    }
    
    public void logout() {
        this.logado = null;
    }
}