package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Acessorio;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Component
@RequestScoped
public class AcessorioDAO 
{ 

    private final Session session;

    public AcessorioDAO(Session session) {
        this.session = session;
    }

    public List<Acessorio> listAll() {
        return this.session.createCriteria(Acessorio.class).list();
    }
    
    public Acessorio load(Long id) {        
        Transaction tx = session.beginTransaction();
        Acessorio a  = (Acessorio) session.load(Acessorio.class, id);
        return a;
    }

    public void update(Acessorio acessorio) {
        Transaction tx = session.beginTransaction();
        session.update(acessorio);
        tx.commit();
    }

    public void delete(Acessorio acessorio) {
        Transaction tx = session.beginTransaction();
        session.delete(acessorio);
        tx.commit();
    }

    public void save(Acessorio acessorio) {
        Transaction tx = session.beginTransaction();
        session.save(acessorio);
        System.out.print(acessorio.toString());
        tx.commit();
    }
}
