package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Kit;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Component
@RequestScoped
public class KitDAO 
{ 

    private final Session session;

    public KitDAO(Session session) {
        this.session = session;
    }

    public List<Kit> listAll() {
        return this.session.createCriteria(Kit.class).list();
    }
    
    public Kit load(Long id) {        
        Transaction tx = session.beginTransaction();
        Kit k  = (Kit) session.load(Kit.class, id);
        return k;
    }

    public void update(Kit kit) {
        Transaction tx = session.beginTransaction();
        session.update(kit);
        tx.commit();
    }

    public void delete(Kit kit) {
        Transaction tx = session.beginTransaction();
        session.delete(kit);
        tx.commit();
    }

    public void save(Kit kit) {
        Transaction tx = session.beginTransaction();
        session.save(kit);
        tx.commit();
    }
}
