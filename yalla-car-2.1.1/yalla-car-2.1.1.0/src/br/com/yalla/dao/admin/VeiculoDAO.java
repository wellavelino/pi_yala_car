/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Fabricante;
import br.com.yalla.entity.Veiculo;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Marlon Sagrilo
 */
@Component
@RequestScoped
public class VeiculoDAO {

    private final Session session;

    public VeiculoDAO(Session session) {
        this.session = session;
    }

    public List<Veiculo> listAll() {
        return this.session.createCriteria(Veiculo.class).list();
    }
    
    public Veiculo load(Long id) {        
        Transaction tx = session.beginTransaction();
        Veiculo c  = (Veiculo) session.load(Veiculo.class, id);
        return c;
    }

    public void update(Veiculo veiculo) {
        Transaction tx = session.beginTransaction();
        session.update(veiculo);
        tx.commit();
    }

    public void delete(Veiculo veiculo) {
        Transaction tx = session.beginTransaction();
        session.delete(veiculo);
        tx.commit();
    }

    public Fabricante veiculoFabricante(Long id)
    {
        Transaction tx = session.beginTransaction();
        Fabricante f  = (Fabricante) session.load(Fabricante.class, id);
        return f;
    }
    
    public void save(Veiculo veiculo) {
        Transaction tx = session.beginTransaction();
        session.save(veiculo);
        tx.commit();
    }
}
