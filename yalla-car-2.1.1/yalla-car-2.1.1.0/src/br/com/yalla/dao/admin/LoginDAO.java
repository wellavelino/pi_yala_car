package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Pessoa;
import br.com.yalla.entity.Usuario;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@Component
@RequestScoped
public class LoginDAO {

    private final Session session;

    public LoginDAO(Session session) {
        this.session = session;
    }
//
//    public void addUserAdmin() {
//        Usuario u = new Usuario();
//        u.setLogin("teste");
//        u.setSenha("yala");
//        if (!this.existeUsuario(u)) {
//            Pessoa p = new Pessoa(Long.MAX_VALUE, "admin", "123456789", "12345678", 'M', "admin@admin.com", Date.from(Instant.EPOCH));
//            Perfil pp =  new Perfil(Integer.SIZE, "admin", "admin", Date.from(Instant.EPOCH));
//            this.session.save(p);
//            this.session.save(pp);
//            u.setAtivo(true);
//            u.setIdPerfil(pp);
//            u.setIdPessoa(p);
//            this.session.save(u);
//        }
//    }
//
//    /**
//     * Nome:Existe Usuario Objetivo: Verificar se exite usuário no banco de
//     * dados
//     *
//     * @param usuario
//     * @return //
//     */
//    public boolean existeUsuario(Usuario usuario) {
//        Usuario encontrado = (Usuario) session.createCriteria(Usuario.class)
//                .add(Restrictions.eq("login", usuario.getLogin()))
//                .uniqueResult();
//        return encontrado != null;
//    }

    /**
     * Nome: Carregar Objetivo: Carregar os dados do usuário
     *
     *
     * @param usuario
     * @return
     */
    public Pessoa carregar(Usuario usuario) {
        return (Pessoa) session.createCriteria(Pessoa.class, "pessoa")
                .createAlias("pessoa.usuariosCollection", "usuario")
                .add(Restrictions.eq("usuario.login", usuario.getLogin()))
                .add(Restrictions.eq("usuario.senha", usuario.getSenha()))
                .add(Restrictions.eq("usuario.ativo", true))
                .uniqueResult();
    }

}
