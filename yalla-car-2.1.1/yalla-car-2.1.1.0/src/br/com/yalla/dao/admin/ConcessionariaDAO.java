/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Concessionaria;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Benjamin
 */
@Component
@RequestScoped
public class ConcessionariaDAO {

    private final Session session;

    public ConcessionariaDAO(Session session) {
        this.session = session;
    }

    public List<Concessionaria> listAll() {
        return this.session.createCriteria(Concessionaria.class).list();
    }  
    
    public Concessionaria load(Long id) {        
        Transaction tx = session.beginTransaction();
        Concessionaria c  = (Concessionaria) session.load(Concessionaria.class, id);
        return c;
    }

    public void update(Concessionaria concessionaria) {
        Transaction tx = session.beginTransaction();
        session.update(concessionaria);
        tx.commit();
    }

    public void delete(Concessionaria concessionaria) {
        Transaction tx = session.beginTransaction();
        session.delete(concessionaria);
        tx.commit();
    }

    public void save(Concessionaria concessionaria) {
        Transaction tx = session.beginTransaction();
        session.save(concessionaria);
        tx.commit();
    }
}
