/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.ModeloCor;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author wellavelino
 */
@Component
@RequestScoped
public class ModeloCorDAO {

    private final Session session;

    public ModeloCorDAO(Session session) {
        this.session = session;
    }

    public List<ModeloCor> getModeloCoresIdModelo(int id) {
        Transaction tx = session.beginTransaction();
        List<ModeloCor> mCores = session.getNamedQuery("ModeloCor.findByIdModelo").setInteger("idModelo", id).list();
        return mCores;
    }

    public ModeloCor getModeloId(int id) {
        Transaction tx = session.beginTransaction();
        ModeloCor m = (ModeloCor) session.getNamedQuery("Modelo.findByIdVeiculo").setInteger("idVeiculo", id).uniqueResult();
        return m;
    }

    public List<ModeloCor> listAll() {
        return this.session.createCriteria(ModeloCor.class).list();
    }

    public ModeloCor load(Long id) {
        Transaction tx = session.beginTransaction();
        ModeloCor c = (ModeloCor) session.load(ModeloCor.class, id);
        return c;
    }

    public void update(ModeloCor modeloCor) {
        Transaction tx = session.beginTransaction();
        session.update(modeloCor);
        tx.commit();
    }

    public void delete(ModeloCor modeloCor) {
        Transaction tx = session.beginTransaction();
        session.delete(modeloCor);
        tx.commit();
    }

    public void save(ModeloCor modeloCor) {
        Transaction tx = session.beginTransaction();
        session.save(modeloCor);
        tx.commit();
    }
}
