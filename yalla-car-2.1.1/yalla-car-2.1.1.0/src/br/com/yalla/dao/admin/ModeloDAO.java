package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Modelo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
@Component
@RequestScoped
public class ModeloDAO {

    private final Session session;

    public ModeloDAO(Session session) {
        this.session = session;
    }
    
    public Modelo getModeloId(int id)
    {
        Transaction tx = session.beginTransaction();
        Modelo m = (Modelo)session.getNamedQuery("Modelo.findByIdVeiculo").setInteger("veiculo", id).uniqueResult();        
        return m;        
    }
    
     public List<Modelo> listAll() {
        return this.session.createCriteria(Modelo.class).list();
    }
    
    public Modelo load(Long id) {        
        Transaction tx = session.beginTransaction();
        Modelo c  = (Modelo) session.load(Modelo.class, id);
        return c;
    }

    public void update(Modelo modelo) {
        Transaction tx = session.beginTransaction();
        session.update(modelo);
        tx.commit();
    }

    public void delete(Modelo modelo) {
        Transaction tx = session.beginTransaction();
        session.delete(modelo);
        tx.commit();
    }

    public void save(Modelo modelo) {
        Transaction tx = session.beginTransaction();
        session.save(modelo);
        tx.commit();
    }
}
