/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import javax.swing.text.MaskFormatter;
import org.hibernate.Session;

/**
 *
 * @author Benjamin
 */
@Component
@RequestScoped
public class UtilDAO {

    private final Session session;

    public UtilDAO(Session session) {
        this.session = session;
    }   
    
    public String[][] getEstados(){       
        String[][] estados = new String [][] {
            { "SELECIONE", ""},
            { "Acre", "AC"},
            { "Alagoas", "AL"},
            { "Amapá", "AP"},
            { "Amazonas", "AM"},
            { "Bahia", "BA"},
            { "Ceará", "CE"},
            { "Distrito Federal", "DF"},
            { "Espírito Santo", "ES"},
            { "Goiás", "GO"},
            { "Maranhão", "MA"},
            { "Mato Grosso", "MT"},
            { "Mato Grosso do Sul", "MS"},
            { "Minas Gerais", "MG"},
            { "Pará", "PA"},
            { "Paraíba", "PB"},
            { "Paraná", "PR"},
            { "Pernambuco", "PE"},
            { "Piauí", "PI"},
            { "Rio de Janeiro", "RJ"},
            { "Rio Grande do Norte", "RN"},
            { "Rio Grande do Sul", "RS"},
            { "Rondônia", "RO"},
            { "Roraima", "RR"},
            { "Santa Catarina", "SC"},
            { "São Paulo", "SP"},
            { "Sergipe", "SE"},
            { "Tocantins", "TO"} };        
        return estados;    
    } 
    
    public String[][] getPerfis(){       
        String[][] perfis = new String [][] {
            { "SELECIONE", ""},
            { "ADMINISTRADOR", "ADM"},
            { "BACKOFFICE", "BKO"},
            { "CONCESSIONARIA", "CNS"}};        
        return perfis;    
    }
    
    public String[][] getSexos(){       
        String[][] sexos = new String [][] {
            { "SELECIONE", ""},
            { "MASCULINO", "M"},
            { "FEMININO", "F"}};        
        return sexos;    
    }
    
    public static String mascaraCpfCnpj(String cnpjCpf, String cnpjFilial,
            String cnpjControl) {

        MaskFormatter formatCnpj;

        MaskFormatter formatCpf;

        if (Integer.valueOf(cnpjFilial) == 0) {

            try {

                formatCpf = new MaskFormatter("###.###.###-##");

                formatCpf.setValueContainsLiteralCharacters(false);

                cnpjCpf = formatCpf.valueToString(cnpjCpf.concat(cnpjControl));

            } catch (java.text.ParseException e) {

                e.printStackTrace();

            }

        } else {

            try {

                formatCnpj = new MaskFormatter("##.###.###/####-##");

                formatCnpj.setValueContainsLiteralCharacters(false);

                cnpjCpf = formatCnpj.valueToString(cnpjCpf.concat(cnpjFilial
                        .concat(cnpjControl)));

            } catch (java.text.ParseException e) {

                e.printStackTrace();

            }

        }

        return cnpjCpf;

    }
}
