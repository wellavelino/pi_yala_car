package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Pessoa;
import java.util.List;
import org.hibernate.Transaction;
import org.hibernate.Session;

@Component
@RequestScoped
public class PessoaDAO {
    
    private final Session session;

    public PessoaDAO(Session session) {
        this.session = session;
    }

    public List<Pessoa> listAll() {
        return this.session.createCriteria(Pessoa.class).list();
    }  
    
    public Pessoa load(Long id) {        
        Transaction tx = session.beginTransaction();
        Pessoa c  = (Pessoa) session.load(Pessoa.class, id);
        return c;
    }

    public void update(Pessoa pessoa) {
        Transaction tx = session.beginTransaction();
        session.update(pessoa);
        tx.commit();
    }

    public void delete(Pessoa pessoa) {
        Transaction tx = session.beginTransaction();
        session.delete(pessoa);
        tx.commit();
    }

    public void save(Pessoa pessoa) {
        Transaction tx = session.beginTransaction();
        session.save(pessoa);
        tx.commit();
    }
}
