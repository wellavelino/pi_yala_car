/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Fabricante;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Benjamin
 */
@Component
@RequestScoped
public class FabricanteDAO {

    private final Session session;

    public FabricanteDAO(Session session) {
        this.session = session;
    }

    public List<Fabricante> listAll() {
        return this.session.createCriteria(Fabricante.class).list();
    }
    
    public Fabricante load(Long id) {        
        Transaction tx = session.beginTransaction();
        Fabricante c  = (Fabricante) session.load(Fabricante.class, id);
        return c;
    }

    public void update(Fabricante fabricante) {
        Transaction tx = session.beginTransaction();
        session.update(fabricante);
        tx.commit();
    }

    public void delete(Fabricante fabricante) {
        Transaction tx = session.beginTransaction();
        session.delete(fabricante);
        tx.commit();
    }

    public void save(Fabricante fabricante) {
        Transaction tx = session.beginTransaction();
        session.save(fabricante);
        tx.commit();
    }
}
