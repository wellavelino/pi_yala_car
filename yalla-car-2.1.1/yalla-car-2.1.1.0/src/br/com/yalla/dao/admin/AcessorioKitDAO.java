package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.AcessorioKit;
import br.com.yalla.entity.Kit;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Component
@RequestScoped
public class AcessorioKitDAO {

    private final Session session;

    public AcessorioKitDAO(Session session) {
        this.session = session;
    }

    public List<Kit> listAll() {
        Transaction tx = session.beginTransaction();
        List<Kit> aK = session.getNamedQuery("AcessorioKit.findAllKit").list();
        return aK;
    }

    public AcessorioKit load(Long id) {
        Transaction tx = session.beginTransaction();
        AcessorioKit c = (AcessorioKit) session.load(AcessorioKit.class, id);
        return c;
    }

    public void update(AcessorioKit concessionaria) {
        Transaction tx = session.beginTransaction();
        session.update(concessionaria);
        tx.commit();
    }

    public void delete(AcessorioKit concessionaria) {
        Transaction tx = session.beginTransaction();
        session.delete(concessionaria);
        tx.commit();
    }

    public void save(AcessorioKit concessionaria) {
        Transaction tx = session.beginTransaction();
        session.save(concessionaria);
        tx.commit();
    }
}
