package br.com.yalla.dao.admin;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.yalla.entity.Usuario;
import java.util.List;
import org.hibernate.Transaction;
import org.hibernate.Session;

@Component
@RequestScoped
public class UsuarioDAO {
    
    private final Session session;

    public UsuarioDAO(Session session) {
        this.session = session;
    }

    public List<Usuario> listAll() {
        return this.session.createCriteria(Usuario.class).list();
    }  
    
    public Usuario load(Long id) {        
        Transaction tx = session.beginTransaction();
        Usuario c  = (Usuario) session.load(Usuario.class, id);
        return c;
    }

    public void update(Usuario usuario) {
        Transaction tx = session.beginTransaction();
        session.update(usuario);
        tx.commit();
    }

    public void delete(Usuario usuario) {
        Transaction tx = session.beginTransaction();
        session.delete(usuario);
        tx.commit();
    }

    public void save(Usuario usuario) {
        Transaction tx = session.beginTransaction();
        session.save(usuario);
        tx.commit();
    }
}
