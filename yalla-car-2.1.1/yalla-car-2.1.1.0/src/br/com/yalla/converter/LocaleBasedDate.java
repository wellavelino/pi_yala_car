/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;
import br.com.caelum.vraptor.converter.ConversionError;
import br.com.caelum.vraptor.core.Localization;
import br.com.caelum.vraptor.ioc.RequestScoped;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Benjamin
 */
@Convert(Date.class)
@RequestScoped
public class LocaleBasedDate implements Converter<Date> {

    private final Localization localization;

    public LocaleBasedDate(Localization localization) {
        this.localization = localization;
    }
    
    @Override
    public Date convert(String value, Class<? extends Date> type, ResourceBundle bundle) {

        if (value == null || value.equals("")) { 
          return null;
        }
        
//        try {  
//            return new SimpleDateFormat("dd/MM/yyyy").parse(value);  
//        } catch (ParseException e) {  
//            return null;  
//        }  

        Locale locale = localization.getLocale();

        if (locale == null) {
          locale = Locale.getDefault();
        }

        DateFormat formatTime = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
        try {
          return formatTime.parse(value);
        } catch (ParseException pe) {
          DateFormat formatDate = DateFormat.getDateInstance(DateFormat.SHORT, locale);
          try {
            return formatDate.parse(value);
          } catch (ParseException pe1) {

            throw new ConversionError(MessageFormat.format(bundle.getString("is_not_a_valid_date"), value));
          }
        }
    }
}