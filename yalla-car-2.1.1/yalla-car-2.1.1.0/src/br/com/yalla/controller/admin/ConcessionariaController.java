/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.ConcessionariaDAO;
import br.com.yalla.dao.admin.FabricanteDAO;
import br.com.yalla.dao.admin.UtilDAO;
import br.com.yalla.entity.Concessionaria;
import br.com.yalla.enums.Perfil;
import java.util.List;

/**
 *
 * @author Benjamin
 */
@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class ConcessionariaController { 

    private final Result result;
    private final Validator validator;
    private final ConcessionariaDAO dao;
    private final FabricanteDAO daoFabricante;
    private final UtilDAO daoUtil;

    public ConcessionariaController(UtilDAO daoU, FabricanteDAO daoF, ConcessionariaDAO dao, Result result, Validator validator) {
        this.daoFabricante = daoF;
        this.daoUtil = daoU;
        this.dao = dao;
        this.result = result;
        this.validator = validator;
    }
    
    @Get("/concessionaria/new")
    public void form() {
        this.appendDataToView();
    }

    @Get("/concessionaria/edit/{id}")
    public Concessionaria edit(Long id) {
        this.appendDataToView();
        return this.dao.load(id);
    }

    @Put("/concessionaria/update/{concessionaria.idConcessionaria}")
    public void update(Concessionaria concessionaria) {
        
        this.validateRequest(concessionaria);
        validator.onErrorForwardTo(this).edit(concessionaria.getId());
        
        this.dao.update(concessionaria);
        result.redirectTo(this).list();
    }

    @Post("/concessionaria/add") 
    public void add(Concessionaria concessionaria) {
        
        this.validateRequest(concessionaria);
        validator.onErrorForwardTo(this).form();
        
        this.dao.save(concessionaria);
        result.redirectTo(this).list();
    }

    @Get("/concessionaria/remove/{id}")
    public void remove(Long id) {
        
        Concessionaria c = this.dao.load(id);
        this.dao.delete(c);
        result.redirectTo(this).list();
    }

    @Get("/concessionaria/list")
    public List<Concessionaria> list() {
        return this.dao.listAll();
    }
    
    private void validateRequest(final Concessionaria concessionaria) {        

        validator.checking(new Validations(){{
            that(concessionaria.getCnpj() != null && !concessionaria.getCnpj().isEmpty(),
                 "errors", "cnpj.required");
            that(concessionaria.getUf() != null && !concessionaria.getUf().equals(""),
                 "errors", "uf.required");
        }});
    }
    
    private void appendDataToView() {        
        this.result.include("comboEstados", this.daoUtil.getEstados()); 
        this.result.include("listaFabricantes", this.daoFabricante.listAll()); 
    }
}
