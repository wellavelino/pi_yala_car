package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.PessoaDAO;
import br.com.yalla.dao.admin.UsuarioDAO;
import br.com.yalla.dao.admin.UtilDAO;
import br.com.yalla.entity.Pessoa;
import br.com.yalla.entity.Usuario;
import br.com.yalla.enums.Perfil;
import java.util.List;

@Resource
@Permission(Perfil.ADMINISTRADOR)
@Path("/admin")
public class UsuarioController {
    
    private final Result result;
    private final Validator validator;
    private final UsuarioDAO dao;
    private final PessoaDAO daoPessoa;
    private final UtilDAO daoUtil;

    public UsuarioController(Result result, Validator validator, UsuarioDAO dao, PessoaDAO daoP, UtilDAO daoU) {
        this.result = result;
        this.validator = validator;
        this.dao = dao;
        this.daoUtil = daoU;
        this.daoPessoa = daoP;
    }
    
    @Get("usuario/new")
    public void form() {
        this.appendDataToView();
    }

    @Get("/usuario/edit/{id}")
    public Usuario edit(Long id) {
        this.appendDataToView();
        return this.dao.load(id);
    }

    @Put("/usuario/update/{usuario.idUsuario}")
    public void update(Pessoa pessoa, Usuario usuario, String tperfil) {
        
        this.setPerfilRequest(usuario, tperfil); 
        
        this.validateRequest(pessoa, usuario);
        validator.onErrorForwardTo(this).edit(usuario.getId());        
        
        this.daoPessoa.update(pessoa);  
        usuario.setPessoa(pessoa);
        this.dao.update(usuario);
        result.redirectTo(this).list();
    }

    @Post("/usuario/add")
    public void add(Pessoa pessoa, Usuario usuario, String tperfil) {
        
        this.setPerfilRequest(usuario, tperfil);
        
        this.validateRequest(pessoa, usuario);
        validator.onErrorForwardTo(this).form();                
        
        this.daoPessoa.save(pessoa);
        usuario.setPessoa(pessoa);
        this.dao.save(usuario);
        result.redirectTo(this).list(); 
    }

    @Get("/usuario/remove/{id}/{idPessoa}")
    public void remove(Long id, Long idPessoa) { 
        
        Usuario c = this.dao.load(id);
        Pessoa  p = this.daoPessoa.load(idPessoa);
        this.dao.delete(c);
        this.daoPessoa.delete(p);
        result.redirectTo(this).list();
    }

    @Get("/usuario/list")
    public List<Usuario> list() {
        return this.dao.listAll();
    }
    
    private void validateRequest(final Pessoa pessoa, final Usuario usuario) {        

        validator.checking(new Validations(){{
            that(usuario.getLogin() != null && !usuario.getLogin().isEmpty(),
                 "errors", "login.required");
            that(pessoa.getNome() != null && !pessoa.getNome().isEmpty(),
                 "errors", "nome.required");
            that(pessoa.getCpf() != null && !pessoa.getCpf().isEmpty(),
                 "errors", "cpf.required");
            that(pessoa.getEmail() != null && !pessoa.getEmail().isEmpty(),
                 "errors", "email.required");
        }});
    }
    
    private void appendDataToView() {        
        this.result.include("comboPerfis", this.daoUtil.getPerfis()); 
        this.result.include("comboSexos", this.daoUtil.getSexos()); 
    }
    
    private void setPerfilRequest(Usuario usuario, String tperfil) {
        
        switch (tperfil) {
            case "ADM":
                usuario.setPerfil(Perfil.ADMINISTRADOR);
                break;
            case "BKO":
                usuario.setPerfil(Perfil.BACKOFFICE);
                break;
            case "CNS":
                usuario.setPerfil(Perfil.CONCESSIONARIA);
                break;
        }
    }
}
