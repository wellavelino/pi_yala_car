/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.view.Results;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.yalla.components.MyImages;
import br.com.yalla.dao.admin.AcessorioDAO;
import br.com.yalla.dao.admin.FabricanteDAO;
import br.com.yalla.dao.admin.ModeloCorDAO;
import br.com.yalla.dao.admin.ModeloDAO;
import br.com.yalla.entity.ArquivoUpload;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Benjamin
 */
@Resource
@Path("/admin")
public class ImagemController {
    
    private final MyImages images;
    private final Result result;
    private final AcessorioDAO daoA;
    private final FabricanteDAO daoF;
    private final ModeloDAO daoM;
    private final ModeloCorDAO daoMC;

    public ImagemController(MyImages images, Result result, AcessorioDAO daoA, FabricanteDAO daoF, ModeloDAO daoM, ModeloCorDAO daoMC) {
        this.images = images;
        this.result = result;
        this.daoA = daoA;
        this.daoF = daoF;
        this.daoM = daoM;
        this.daoMC = daoMC;
    }  

    @Post("/image/upload")
    public void upload(UploadedFile imagem) {  
        
        ArquivoUpload up = new ArquivoUpload(); 
        List<ArquivoUpload> list = new ArrayList<>(); 
        
        String fileName = this.getFilename(imagem);        
        up.setName(fileName);  
        up.setSize(100);  
        up.setUrl("none");  
        up.setThumbnailUrl("none");  
        up.setDeleteUrl("#");   
        up.setDeleteType("DELETE");       
        
        list.add(up);  
        images.upload(imagem, fileName); 
        result.use(Results.json()).withoutRoot().from(list).serialize();
    }
    
    @Get("/fabricante/{id}/imagem")
    public File downloadFabricante(Long id) {     
          return images.download(this.daoF.load(id));
    }
    
    @Get("/acessorio/{id}/imagem")
    public File downloadAcessorio(Long id) {        
          return images.download(this.daoA.load(id));
    }
    
    @Get("/modelo/{id}/imagem")
    public File downloadModelo(Long id) {        
          return images.download(this.daoM.load(id));
    }
    
    @Get("/modelocor/{id}/imagem")
    public File downloadModeloCor(Long id) {        
          return images.download(this.daoMC.load(id));
    }
    
    public boolean deleteFile(String imagem) {     
          return images.delete(imagem);
    }
    
    private String getFilename(UploadedFile files) {        
        String fileName = files.getFileName();
        int start = fileName.lastIndexOf(".");
        String extensao = (start > 0) ? fileName.substring(start) : ".jpg";                
        return UUID.randomUUID().toString() + extensao; 
    }
    
}
