/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.FabricanteDAO;
import br.com.yalla.entity.Fabricante;
import br.com.yalla.enums.Perfil;
import java.util.List;

/**
 *
 * @author Benjamin 
 */
@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class FabricanteController {

    private final Result result;
    private final Validator validator;
    private final FabricanteDAO dao;
    private final ImagemController imagemController;

    public FabricanteController(FabricanteDAO dao, Result result, Validator validator, ImagemController imagemController) {
        this.dao = dao;
        this.result = result;
        this.validator = validator;
        this.imagemController = imagemController;
    }
    
    @Get("/fabricante/new")
    public void form() {         
    }

    @Get("/fabricante/edit/{id}")
    public Fabricante edit(Long id) {
        return this.dao.load(id);
    }

    @Put("/fabricante/update/{fabricante.idFabricante}")
    public void update(Fabricante fabricante, String oldImagem) {
        
        this.validateRequest(fabricante);
        validator.onErrorUsePageOf(this).edit(fabricante.getId());
        
        if (!oldImagem.equals(fabricante.getImagem())) {
            this.imagemController.deleteFile(oldImagem);   
        }
        
        this.dao.update(fabricante);
        result.redirectTo(this).list();
    }

    @Post("/fabricante/add")
    public void add(Fabricante fabricante) {
        
        this.validateRequest(fabricante);
        validator.onErrorUsePageOf(this).form();
        
        this.dao.save(fabricante);
        result.redirectTo(this).list();
    }

    @Get("/fabricante/remove/{id}")
    public void remove(Long id) {
        
        Fabricante f = this.dao.load(id);        
        this.imagemController.deleteFile(f.getImagem());
        this.dao.delete(f);
        result.redirectTo(this).list();
    }

    @Get("/fabricante/list")
    public List<Fabricante> list() {
        return this.dao.listAll();
    }
    
    private void validateRequest(final Fabricante fabricante) {        

        validator.checking(new Validations(){{
            that(fabricante.getNome() != null && !fabricante.getNome().isEmpty(),
                 "errors", "nome.required");
            that(fabricante.getImagem() != null && !fabricante.getImagem().isEmpty(),
                 "errors", "imagem.required");
        }});
    }
}
