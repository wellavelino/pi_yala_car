/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.ModeloDAO;
import br.com.yalla.dao.admin.VeiculoDAO;
import br.com.yalla.entity.Modelo;
import br.com.yalla.enums.Perfil;
import java.util.List;

/**
 *
 * @author Benjamin
 */
@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class ModeloController {

    private final Result result;
    private final Validator validator;
    private final ModeloDAO dao;
    private final VeiculoDAO daoVeiculo;
     private final ImagemController imagemController;

    public ModeloController(ModeloDAO dao, Result result, Validator validator,VeiculoDAO veiculo, ImagemController imagemController) {
        this.dao = dao;
        this.result = result;
        this.validator = validator;
        this.daoVeiculo = veiculo;
         this.imagemController = imagemController;
    }

    @Get("/modelo/new")
    public void form() {
        this.appendDataToView();
    }
    
     @Get("/modelo/list")
    public List<Modelo> list() {
        return this.dao.listAll();
    }
    
     @Post("/modelo/add")
    public void add(Modelo modelo) {
        
        this.validateRequest(modelo);
        validator.onErrorForwardTo(this).form();
        
        this.dao.save(modelo);
        result.redirectTo(this).list();
    }
    
      @Get("/modelo/remove/{id}")
    public void remove(Long id) {
        
        Modelo c = this.dao.load(id);
        this.dao.delete(c);
        result.redirectTo(this).list();
    }
    
     @Get("/modelo/edit/{id}")
    public Modelo edit(Long id) {
        this.appendDataToView();
        return this.dao.load(id);
    }
    
    @Put("/modelo/update/{modelo.idModelo}")
    public void update(Modelo modelo,String oldImagem) {
        
        this.validateRequest(modelo);
        validator.onErrorForwardTo(this).edit(modelo.getId());
        
        if (!oldImagem.equals(modelo.getImagem())) {
            this.imagemController.deleteFile(oldImagem);   
        }
        
        this.dao.update(modelo);
        result.redirectTo(this).list();
    }
    
     private void validateRequest(final Modelo modelo) {

        validator.checking(new Validations() {
            {
                that(modelo.getNome() != null && !modelo.getNome().isEmpty(),
                        "errors", "nome.required");
                that(modelo.getPreco()!=null && !modelo.getPreco().equals("") ,
                        "errors", "preco.required");
                 that(modelo.getAnoFabricacao()!= null && !modelo.getAnoFabricacao().equals(""),
                        "errors", "anoFabricacao.required");
                 
            }
        });
    }
    
    private void appendDataToView() {

        this.result.include("listaVeiculos", this.daoVeiculo.listAll());
    }
}
