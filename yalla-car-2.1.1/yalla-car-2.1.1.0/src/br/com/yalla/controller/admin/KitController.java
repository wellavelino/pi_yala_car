/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.AcessorioDAO;
import br.com.yalla.dao.admin.AcessorioKitDAO;
import br.com.yalla.dao.admin.KitDAO;
import br.com.yalla.entity.Acessorio;
import br.com.yalla.entity.AcessorioKit;
import br.com.yalla.entity.Kit;
import br.com.yalla.enums.Perfil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author alberto
 */
@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class KitController {

    private final KitDAO dao;
    private final AcessorioDAO daoAcessorio;
    private final AcessorioKitDAO daoAcessorioKit;
    private final Result result;
    private final Validator validator;

    public KitController(KitDAO dao, Result result, Validator validator, AcessorioDAO daoAcessorio, AcessorioKitDAO daoAcessorioKit) {
        this.daoAcessorioKit = daoAcessorioKit;
        this.daoAcessorio = daoAcessorio;
        this.dao = dao;
        this.result = result;
        this.validator = validator;
    }

    @Get("/kit/new")
    public void form() {
        this.appendDataToView();
    }

    @Get("/kit/edit/{id}")
    public Kit edit(Long id) {
        this.appendDataToView();
        return this.dao.load(id);
    }

    @Put("/kit/update/{kit.idKit}")
    public void update(Kit kit, Acessorio[] acessorio) {
        
        this.validateRequest(kit);
        validator.onErrorForwardTo(this).edit(kit.getId());
        
        this.dao.update(kit);
        result.redirectTo(this).list();
    }

    @Post("/kit/add") 
    public void add(Kit kit, Acessorio[] acessorio) {         

        this.validateRequest(kit);
        this.validateAcessorio(acessorio);    
        
        // seta os acessorios enviados do formulario
        Collection<AcessorioKit> ack = new ArrayList<>();
        for (Acessorio a : acessorio) {
            AcessorioKit ak = new AcessorioKit();
            ak.setAcessorio(a);
            ack.add(ak);
        }
        
        //kit.setAcessorioKitsCollection(ack);          
        validator.onErrorForwardTo(this).form();    
        
        // salva o kit e suas relacoes
        this.dao.save(kit);        
        for (AcessorioKit _ak : ack) {
            _ak.setKit(kit);
            daoAcessorioKit.save(_ak);            
        }
        
        result.redirectTo(this).list();
    }

    @Get("/kit/remove/{id}")
    public void remove(Long id) {
        
        Kit c = this.dao.load(id);
        this.dao.delete(c);
        result.redirectTo(this).list();
    }

    @Get("/kit/list")
    public List<Kit> list() {
        return this.dao.listAll();
    }
    
    private void validateRequest(final Kit kit) {        

        validator.checking(new Validations(){{
            that(kit.getNome() != null && !kit.getNome().isEmpty(),
                 "errors", "nome.required");
            that(kit.getDesconto() != null && !kit.getDesconto().toString().isEmpty(),
                 "errors", "desconto.required");
            that(kit.getPrecoTotal() != null && !kit.getPrecoTotal().toString().isEmpty(),
                 "errors", "precoTotal.required");
            that(kit.getBasePreco() != null && !kit.getBasePreco().toString().isEmpty(),
                 "errors", "basePreco.required");
        }});
    }
    
    private void validateAcessorio(Acessorio[] acessorio) {        
        
        if (acessorio.length <= 0) {
            validator.add(new ValidationMessage("Escolha ao menos um acessorio",
                                                "acessorio.required"));
        }

        if (acessorio.length > 5) {
            validator.add(new ValidationMessage("Um kit não pode conter mais de cinco acessórios",
                                                "acessorio.overflow"));
        }
    }
    
    private void appendDataToView() {
        this.result.include("listaAcessorios", this.daoAcessorio.listAll()); 
    }
}
