/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.ModeloCorDAO;
import br.com.yalla.dao.admin.ModeloDAO;
import br.com.yalla.entity.ModeloCor;
import br.com.yalla.enums.Perfil;
import java.util.List;

/**
 *
 * @author wellavelino
 */
@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class ModeloCorController {

    private final Result result;
    private final Validator validator;
    private final ModeloCorDAO dao;
    private final ModeloDAO daoM;
    private final ImagemController imagemController;
    
    public ModeloCorController(ModeloCorDAO dao, Result result, Validator validator,ModeloDAO daoM,ImagemController imagemController) {
        this.dao = dao;
        this.result = result;
        this.validator = validator;
        this.daoM = daoM;
        this.imagemController = imagemController;
    }
 
    @Get("/modelocor/new")
    public void form() {
        this.appendDataToView();
    }
    
     @Get("/modelocor/list")
    public List<ModeloCor> list() {
        return this.dao.listAll();
    }
    
     @Post("/modelocor/add")
    public void add(ModeloCor modeloCor) {
        
        this.validateRequest(modeloCor);
        validator.onErrorForwardTo(this).form();
        
        this.dao.save(modeloCor);
        result.redirectTo(this).list();
    }
    
      @Get("/modelocor/remove/{id}")
    public void remove(Long id) {
        
        ModeloCor c = this.dao.load(id);
        this.dao.delete(c);
        result.redirectTo(this).list();
    }
    
     @Get("/modelocor/edit/{id}")
    public ModeloCor edit(Long id) {
        this.appendDataToView();
        return this.dao.load(id);
    }
    
    @Put("/modelocor/update/{modelo.idModelo}")
    public void update(ModeloCor modeloCor,String oldImagem) {
        
        this.validateRequest(modeloCor);
        validator.onErrorForwardTo(this).edit(modeloCor.getId());
        
        if (!oldImagem.equals(modeloCor.getImagem())) {
            this.imagemController.deleteFile(oldImagem);   
        }
        
        this.dao.update(modeloCor);
        result.redirectTo(this).list();
    }
    
     private void validateRequest(final ModeloCor modeloCor) {

        validator.checking(new Validations() {
            {
                that(modeloCor.getNome() != null && !modeloCor.getNome().isEmpty(),
                        "errors", "nome.required");
            }
        });
    }
    
    private void appendDataToView() {

        this.result.include("listaModelos", this.daoM.listAll());
    }

}
