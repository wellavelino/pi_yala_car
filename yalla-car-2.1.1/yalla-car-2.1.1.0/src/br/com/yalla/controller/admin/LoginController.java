package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Public;
import br.com.yalla.dao.admin.LoginDAO;
import br.com.yalla.entity.Usuario;
import br.com.yalla.session.ValidaLogin;
import br.com.yalla.entity.Pessoa;

/**
 *
 * @author Marlon Sagrilo
 */
@Resource
@Path("/admin")
public class LoginController {
    
    private final LoginDAO dao;
    private final Result result;
    private final Validator validator;
    private final ValidaLogin loginValidado;

    /**
     *
     * @param dao
     * @param result
     * @param validator
     * @param login
     */
    public LoginController(LoginDAO dao, Result result,
            Validator validator, ValidaLogin login) {
        this.dao = dao;
        this.result = result;
        this.validator = validator;
        this.loginValidado = login;
    }
    
    @Public
    @Post("/login/logon")
    public void login(Usuario usuario) {
//        this.dao.addUserAdmin();
        this.validateRequest(usuario);
        validator.onErrorUsePageOf(LoginController.class).form();

        final Pessoa carregado = dao.carregar(usuario);
        
        if (carregado == null) {
            validator.add(new ValidationMessage("Login e/ou senha inválidos ou Usuário desativado",
                                                "usuario.login"));
        }
        
        validator.onErrorUsePageOf(LoginController.class).form();
        loginValidado.login(carregado);
        result.redirectTo(WelcomeController.class).form();
    }

    @Get("/login/logout")
    public void logout() {
        loginValidado.logout();
        result.redirectTo(LoginController.class).form();
    }

    @Public
    @Get("/login/form")
    public void form() {
    }

    private void validateRequest(final Usuario usuario) {

        validator.checking(new Validations() {{
            that(usuario.getLogin() != null && !usuario.getLogin().isEmpty(),
                    "errors", "login.required");
            that(usuario.getSenha() != null && !usuario.getSenha().isEmpty(),
                    "errors", "senha.required");
        }});
    }
}
