package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.AcessorioDAO;
import br.com.yalla.entity.Acessorio;
import br.com.yalla.enums.Perfil;
import java.util.List;

@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class AcessorioController {

    private final Result result;
    private final Validator validator;
    private final AcessorioDAO dao;
    private final ImagemController imagemController;

    public AcessorioController(AcessorioDAO dao, Result result, Validator validator, ImagemController imagemController) {
        this.dao = dao;
        this.result = result;
        this.validator = validator;
        this.imagemController = imagemController;
    }

    @Get("/acessorio/new")
    public void form() {
    }

    @Get("/acessorio/edit/{id}")
    public Acessorio edit(Long id) {
        return this.dao.load(id);
    }

    @Put("/acessorio/update/{acessorio.idAcessorio}")
    public void update(Acessorio acessorio, String oldImagem) {
        
        this.validateRequest(acessorio);
        validator.onErrorForwardTo(this).edit(acessorio.getId());
        
        if (!oldImagem.equals(acessorio.getImagem())) {
            this.imagemController.deleteFile(oldImagem);   
        }
        
        this.dao.update(acessorio);
        result.redirectTo(this).list();
    }

    @Post("/acessorio/add")
    public void add(Acessorio acessorio) {
        
        this.validateRequest(acessorio);
        validator.onErrorUsePageOf(this).form();

        this.dao.save(acessorio);
        result.redirectTo(this).list();
    }

    @Get("/acessorio/remove/{id}")
    public void remove(Long id) {
        
        Acessorio c = this.dao.load(id);
        this.imagemController.deleteFile(c.getImagem());
        this.dao.delete(c);
        result.redirectTo(this).list();
    }

    @Get("/acessorio/list")
    public List<Acessorio> list() {
        return this.dao.listAll();
    }

    private void validateRequest(final Acessorio acessorio) {

        validator.checking(new Validations() {
            {
                that(acessorio.getNome() != null && !acessorio.getNome().isEmpty(),
                        "errors", "nome.required");
                that(acessorio.getCategoria() != null && !acessorio.getCategoria().equals(""),
                        "errors", "categoria.required");
            }
        });
    }
    
    private void appendDataToView() {}
}

