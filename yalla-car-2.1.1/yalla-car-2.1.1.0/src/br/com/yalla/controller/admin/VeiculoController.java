/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.controller.admin;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.yalla.annotation.Permission;
import br.com.yalla.dao.admin.FabricanteDAO;
import br.com.yalla.dao.admin.VeiculoDAO;
import br.com.yalla.entity.Veiculo;
import br.com.yalla.enums.Perfil;
import java.util.List;

/**
 *
 * @author Benjamin
 */
@Resource
@Permission({Perfil.ADMINISTRADOR, Perfil.BACKOFFICE})
@Path("/admin")
public class VeiculoController {

    private final Result result;
    private final Validator validator;
    private final FabricanteDAO daoFabricante;
    private final VeiculoDAO dao;

    public VeiculoController(VeiculoDAO dao, Result result, Validator validator, FabricanteDAO fabricante) {
        this.dao = dao;
        this.result = result;
        this.validator = validator;
        this.daoFabricante = fabricante;
    }

    @Get("/veiculo/new")
    public void form() {
        this.appendDataToView();
    }
    
    @Get("/veiculo/list")
    public List<Veiculo> list() {
        return this.dao.listAll();
    }
    
    @Post("/veiculo/add")
    public void add(Veiculo veiculo) {
        
        this.validateRequest(veiculo);
        validator.onErrorForwardTo(this).form();
        
        this.dao.save(veiculo);
        result.redirectTo(this).list();
    }
    
    @Get("/veiculo/remove/{id}")
    public void remove(Long id) {
        
        Veiculo c = this.dao.load(id);
        this.dao.delete(c);
        result.redirectTo(this).list();
    }
    
    @Get("/veiculo/edit/{id}")
    public Veiculo edit(Long id) {
        this.appendDataToView();
        return this.dao.load(id);
    }
    
    @Put("/veiculo/update/{veiculo.idVeiculo}")
    public void update(Veiculo veiculo) {
        
        this.validateRequest(veiculo);
        validator.onErrorForwardTo(this).edit(veiculo.getId());
        
        this.dao.update(veiculo);
        result.redirectTo(this).list();
    }
    
    private void validateRequest(final Veiculo veiculo) {

        validator.checking(new Validations() {
            {
                that(veiculo.getNome() != null && !veiculo.getNome().isEmpty(),
                        "errors", "nome.required");
                that(veiculo.getCategoria() != null && !veiculo.getCategoria().equals(""),
                        "errors", "categoria.required");
                that(veiculo.getQuantidadePorta()!=null && !veiculo.getQuantidadePorta().equals("") ,
                        "errors", "qtdePortas.required");
                 that(veiculo.getFabricante()!= null && !veiculo.getFabricante().equals(""),
                        "errors", "fabricante.required");
            }
        });
    }
    
    private void appendDataToView() {

        this.result.include("listaFabricantes", this.daoFabricante.listAll());
    }
}
