package br.com.yalla.controller.client;

import br.com.yalla.annotation.Public;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.yalla.dao.admin.AcessorioDAO;
import br.com.yalla.dao.admin.AcessorioKitDAO;
import br.com.yalla.dao.admin.FabricanteDAO;
import br.com.yalla.dao.admin.KitDAO;
import br.com.yalla.dao.admin.ModeloCorDAO;
import br.com.yalla.dao.admin.VeiculoDAO;
import br.com.yalla.entity.Acessorio;
import br.com.yalla.entity.Fabricante;
import br.com.yalla.entity.Kit;
import br.com.yalla.entity.ModeloCor;
import java.util.List;

@Resource
public class IndexController {

    private final Result result;
    private final FabricanteDAO fdao;
    private final VeiculoDAO vDAO;
    private final ModeloCorDAO mCoresDAO; 
    private final AcessorioDAO aDAO;
    private final KitDAO kDAO;
    private final AcessorioKitDAO akDAO;
    

    public IndexController(Result result, FabricanteDAO fdao, VeiculoDAO vDAO, ModeloCorDAO mCoresDAO, AcessorioDAO aDAO, KitDAO kDAO,AcessorioKitDAO akDAO) {
        this.result = result;
        this.fdao = fdao;
        this.vDAO = vDAO;
        this.mCoresDAO = mCoresDAO;
        this.aDAO = aDAO;
        this.kDAO = kDAO;
        this.akDAO = akDAO;
    }

    @Public
    @Path("/")
    public void index() {
        
    }
    
    @Public
    @Path("/cadastrocliente")
    public void cadastrocliente(){
        
    
    }

    @Public
    @Path("/seleciona")
    public List<Fabricante> seleciona() {
        List<Fabricante> list = this.fdao.listAll();
        return list;
    }

    @Public
    @Get("/veiculoConf/{id}")
    public void veiculo(Long id) {
        Fabricante list =  this.vDAO.veiculoFabricante(id);
        result.include("listVeiculos", list.getVeiculosCollection());
    }
    
    @Public
    @Get("/montVeiculo/{id}")
    public void conf(int id) {
        List<ModeloCor> modeloCores =  this.mCoresDAO.getModeloCoresIdModelo(id);
        List<Kit> kits = this.akDAO.listAll();
        List<Acessorio> acessorios = this.aDAO.listAll();
        result.include("modelos", modeloCores).include("acessorios", acessorios).include("kits", kits);
    }
}
