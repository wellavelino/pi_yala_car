package br.com.yalla.controller.client;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.yalla.annotation.Public;
import br.com.yalla.dao.admin.FabricanteDAO;

@Resource
@Path("/client")
public class ConfVeiculoController {

    private final Result result;
    private final FabricanteDAO fdao;

    public ConfVeiculoController(Result result, FabricanteDAO fdao) {
        this.result = result;
        this.fdao = fdao;
    }

}
