/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.components;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author Benjamin
 */
@Component
public class SessionCreator implements ComponentFactory<Session> {

    private final SessionFactory factory;
    private Session session;


    public SessionCreator(SessionFactory factory) {
        System.out.println("SessionCreator");
        this.factory = factory;
    }

    @PostConstruct
    public void open() {
        this.session = factory.openSession();
    }

    @Override
    public Session getInstance() {
        return factory.openSession();
    }
    
    @PreDestroy
    public void close() {
        this.session.close();
    }
}
