/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.components;

import br.com.yalla.interfaces.ImageInterface;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.ioc.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Benjamin
 */
@Component
public class MyImages {
    
    static final String UPLOAD_FOLDER  = "WebContent/uploads";
    private String path;

    public MyImages(ServletContext context) {
        String realPath = context.getRealPath("/");         
        this.path = realPath.substring(0, realPath.length()-10) + UPLOAD_FOLDER;     
    } 

    public void upload(UploadedFile files, String fileName) {
        
	InputStream stream = files.getFile(); 
        
        File file = new File(this.path, fileName);   
        
	try {
            file.createNewFile();
            IOUtils.copy(stream, new FileOutputStream(file));
	} catch (IOException e) {
            throw new RuntimeException("Erro ao copiar imagem", e);   
	} 
    }

    public File download(ImageInterface objImagem) { 
        return new File(this.path, objImagem.getImagem());
    }

    public boolean delete(String imagem) { 
        File file = new File(this.path, imagem);
        
        if (file.exists()) {
          System.gc();
          file.deleteOnExit();
          file.delete();
          return true;
        }
        
        return false;
    }
}
