/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.components;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author Benjamin
 */
@Component
@ApplicationScoped
public class SessionCreatorFactory implements ComponentFactory<SessionFactory> {
    
    private SessionFactory factory;

    @PostConstruct
    public void open() {        
        System.out.println("SessionCreatorFactory->open");
        AnnotationConfiguration configuration = new AnnotationConfiguration();
        configuration.configure();        
        this.factory = configuration.buildSessionFactory();
    }    

    @Override
    public SessionFactory getInstance() {
        return this.factory;
    }
    
    @PreDestroy
    public void close() {
        this.factory.close();
    }
}
