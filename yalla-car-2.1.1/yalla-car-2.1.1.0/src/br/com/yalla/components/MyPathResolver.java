package br.com.yalla.components;


import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.view.PathResolver;

/**
 *
 * @author Benjamin
 */
@Component
public class MyPathResolver implements PathResolver {

    static final String PATH_NAME  = "/WEB-INF/jsp/";
    static final String VIEW_SUFIX = ".jsp";
    static final String CLASS_SUFIX = "Controller";

    @Override
    public String pathFor(ResourceMethod method) {
         System.out.println("MyPathResolver-> " + method.toString());
        
        final Class<?> _class = method.getResource().getType();
        final String className = _class.getSimpleName();
        final Package pkg = _class.getPackage();
        
        final StringBuilder s = new StringBuilder(60);
        s.append(PATH_NAME);
        s.append(pkg.getName().substring(pkg.getName().lastIndexOf(".") + 1));
        s.append("/");
        s.append(className.substring(0, className.indexOf(CLASS_SUFIX)).toLowerCase());
        s.append("/");
        s.append(method.getMethod().getName().toLowerCase());
        s.append(VIEW_SUFIX);

        return s.toString();
    }
}