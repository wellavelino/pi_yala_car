/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.yalla.interceptor;

import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.view.Results;
import br.com.yalla.annotation.Permission;
import br.com.yalla.annotation.Public;
import br.com.yalla.controller.admin.ErroController;
import br.com.yalla.enums.Perfil;
import br.com.yalla.session.ValidaLogin;
import java.util.Arrays;
import java.util.Collection;

/**
 *
 * @author Benjamin
 */
@Intercepts
public class PermissionInteceptor implements Interceptor {
    
    private final ValidaLogin pessoa;
    private final Result result;
    
    public PermissionInteceptor(ValidaLogin pessoa, Result result) {
        this.pessoa = pessoa;
        this.result = result;
    }    
    
    @Override
    public boolean accepts(ResourceMethod method) {
        return
            !(method.getMethod().isAnnotationPresent(Public.class) ||
              method.getResource().getType().isAnnotationPresent(Public.class));
    }
    
    @Override
    public void intercept(InterceptorStack stack, ResourceMethod method, Object resource) {
        
        Permission methodPermission = method.getMethod().getAnnotation(Permission.class);
        Permission controllerPermission = method.getResource().getType().getAnnotation(Permission.class);

        if (this.hasAccess(methodPermission) && this.hasAccess(controllerPermission)) {
           stack.next(method, resource);
        } else {
           result.use(Results.logic()).redirectTo(ErroController.class).error403();
        }
    }
    
    private boolean hasAccess(Permission permission) {
        
        if (permission == null) {
           return true;
        }

        Collection<Perfil> perfilList = Arrays.asList(permission.value());
        return perfilList.contains(this.pessoa.getUser().getPerfil());
    }
    
}
