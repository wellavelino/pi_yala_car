
package br.com.yalla.interceptor;

import br.com.yalla.session.ValidaLogin;
import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.yalla.annotation.Public;
import br.com.yalla.controller.admin.LoginController;

/**
 *
 * @author Marlon Sagrilo
 */
@Intercepts
public class AuthInterceptor implements Interceptor {
    
    private final ValidaLogin pessoa;
    private final Result result;
    
    public AuthInterceptor (ValidaLogin pessoa, Result result){
        this.pessoa = pessoa;
        this.result = result;
    }
    
    @Override
    public boolean accepts(ResourceMethod method) {
        return /*!this.usuario.isLogado()&& method.containsAnnotation(Public.class)*/
            !(method.getMethod().isAnnotationPresent(Public.class) ||
              method.getResource().getType().isAnnotationPresent(Public.class));
    }    

    @Override
    public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {
        if (this.pessoa.isLogado()) {
            stack.next(method, resourceInstance);
        } else {
            result.redirectTo(LoginController.class).form();
        }
    }    
}
